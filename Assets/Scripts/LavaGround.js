﻿#pragma strict
private var deltas : float[] = new float[10];

function Awake ()
{
	for (var i = 0; i < 10; i++)
		deltas[i] = 0;
}

function OnTriggerEnter (other : Collider)
{
	if (other.gameObject.name == "Character(Clone)" && other.networkView.isMine)
	{
		var player = other.gameObject.GetComponent(PlayerManager);		
		player.ApplyDelta(PlayerManager.stat.hpRegem, -deltas[player.heroNumber], 0);
	}
}

function OnTriggerExit (other : Collider)
{
	if (other.gameObject.name == "Character(Clone)" && other.networkView.isMine)
	{
		var player = other.gameObject.GetComponent(PlayerManager);
		var delta = -0.05 * player.maxH;
		if (delta > -10)
			delta = -10;
		deltas[player.heroNumber] = delta;
		player.ApplyDelta(PlayerManager.stat.hpRegem, delta, 0);
	}
}