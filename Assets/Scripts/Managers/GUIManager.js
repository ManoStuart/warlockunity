#pragma strict
#pragma implicit
#pragma downcast

private var spells : SpellsManager;
private var playerStats : PlayerManager;
private var inputManager : InputManager;
static var gui : GUIManager;

private var myStyle : GUIStyle = new GUIStyle();
private var boldtext = new GUIStyle ();

// Button Select Shit
private var hover : int;
private var hoverTime : float;
private var hoverMax : float;
private var gotInput : boolean;
private var safeGround : boolean;
private var shopping : boolean;

// Default Textures
var cdTexture : Texture;
var selectBox : Texture;
var spellBox : Texture;
var hpBoxTexture : Texture;
var currentHpTexture : Texture;
var expBoxTexture : Texture;
var currentExpTexture : Texture;
var hudTexture : Texture;

// Spells/Items Icons
private var spellsTextures : Texture[] = new Texture[16];  /// Spels (0-7), Items(7-12),Item Selection (12-16)
private var spellsPos : Rect[] = new Rect[16];
private var spellsDiscription : String[] = new String[16];
private var spellsName : String[] = new String[16];
private var spellsCost : int[] = new int[16];

// Item Selection
private var itemSelected : boolean;
private var nextLvls : Array;
private var curSlot : int = -1;
private var curLvl : int;
private var itemsX : int;

// Spells Selection
private var cdPos : Rect[] = new Rect[7];
private var selectPos : Rect;
private var spellsX : int;
private var spellsY : int;

// Health / Exp icons
private var currentHpPos : Rect;
private var expBoxPos : Rect;
private var currentExpPos : Rect;
private var hpBoxPosHeroes : Rect[] = new Rect[10];
private var currentHpPosHeroes : Rect[] = new Rect[10];
private var hpXLength : float = 30;
private var hpYLength : float = 4.2;
private var hud : Rect;

// Text Shit
private var hpText : String;
private var hpTextPos : Rect;
private var regenText : String;
private var regenTextPos : Rect;
private var goldTextPos : Rect;
private var expTextPos : Rect;
private var lvlTextPos : Rect;

function Awake ()
{
	enabled = false;
	GUIManager.gui = this;
	boldtext.fontStyle = FontStyle.Bold;
	myStyle.alignment = TextAnchor.MiddleCenter;
	var i : int;
	
	hud = Rect(Screen.width /2 - 330, Screen.height - 155, 660, 155);

	// Spells
	spellsX = Screen.width/2 - 328;
	spellsY = Screen.height - 12;
	selectPos = Rect (0, spellsY, 54, 0);
	for (i = 0; i < 7; i++)
	{
		spellsPos[i] = Rect(spellsX + i * 81.1, spellsY - 71, 54, 71);
		cdPos[i] = Rect (spellsX + i * 81.1, spellsY, 54, 0);
	}
	
	// HP
	currentHpPos = Rect (Screen.width/2 - 326, Screen.height - 125, 582, 24);
	expBoxPos = Rect (Screen.width/2 + 263, Screen.height - 154, 28, 154);
	currentExpPos = Rect (Screen.width/2 + 263, Screen.height, 28, 0);
	
	for (i = 0; i < 10; i++)
	{
		currentHpPosHeroes[i] = Rect(0, 0, 0, hpYLength);
		hpBoxPosHeroes[i] = Rect(0, 0, 0, hpYLength);
	}	
	
	// Text
	regenTextPos = Rect(0, Screen.height - 123, 0, 20);
	hpTextPos = Rect(0, Screen.height - 123, 0, 20);
	goldTextPos = Rect(Screen.width / 2 + 298, Screen.height - 72, 30, 20);
	expTextPos = Rect(Screen.width / 2 + 298, Screen.height - 50, 30, 20);
	lvlTextPos = Rect(Screen.width / 2 + 238, Screen.height - 150, 30, 20);
}

function Start ()
{
	var i : int;
	for (i = 0; i < 7; i++)
		spellsTextures[i] = spells.GetSpellTexture(i);
	
	itemsX = Screen.width/2 + 40;
	// Items
	for (i = 7; i < 12; i++)
	{
		var aux : System.Object[] = spells.GetCurItem(i - 7);
		spellsTextures[i] = aux[0];
		spellsName[i] = aux[1];
		spellsCost[i] = aux[2];
		spellsDiscription[i] = aux[3];
		spellsPos[i] = Rect(Screen.width/2 - 120 + 40 * (i - 7), Screen.height - 200, 32, 32);
	}
	spellsPos[12] = Rect(Screen.width/2 - 120 + 40 * 5, Screen.height - 200, 32, 32);
	for (i = 13; i < 16; i++)
		spellsPos[i] = Rect(Screen.width/2 + 40 + 40 * (i - 13), Screen.height - 240, 32, 32);
}

function SetItem (i : int, aux : System.Object[])
{
	i += 7;
	spellsTextures[i] = aux[0];
	spellsName[i] = aux[1];
	spellsCost[i] = aux[2];
	spellsDiscription[i] = aux[3];
}

function SetHeroHp (pos : Vector3, heroNumber : int, normalizedHp : float, visible : boolean)
{
	if (!visible)
	{
		currentHpPosHeroes[heroNumber].width = 0;
		return;
	}
	
	var aux = Camera.main.WorldToScreenPoint(pos);
	
	if (aux.y > Screen.height || aux.y < -7 || aux.x > Screen.width + hpXLength/2 || aux.x < -hpXLength/2)
		currentHpPosHeroes[heroNumber].width = 0;
	else
	{
		currentHpPosHeroes[heroNumber].Set(aux.x - hpXLength/2, Screen.height - aux.y, hpXLength * normalizedHp, hpYLength);
		hpBoxPosHeroes[heroNumber].Set(aux.x - hpXLength/2, Screen.height - aux.y, hpXLength, hpYLength);
	}
}

function SetHpLenght (newXLength : float, newYLength)
{
	hpXLength = newXLength;
	hpYLength = newYLength;
}

function SetPlayer (player : GameObject)
{
	spells = player.GetComponent(SpellsManager);
	playerStats = player.GetComponent(PlayerManager);
	inputManager = player.GetComponent(InputManager);
	enabled = true;
}

function SelectSpell (i : int)
{
	if (i == -1)
		selectPos.height = 0;
	else
	{
		selectPos.height = -71;
		selectPos.x = spellsX + i * 81.1;
	}
}

function NewSpell (i : int, text : Texture)
{
	spellsTextures[i] = text;
}

function SetItemSelectionTree (item : int, at : int, cur : int)
{
	var aux : Array[] = spells.PossibleItemTree(item, at);
	curSlot = item;
	curLvl = at;
	
	nextLvls = aux[0];
	spellsTextures[12] = spellsTextures[cur];
	spellsDiscription[12] = spellsDiscription[cur];
	spellsName[12] = spellsName[cur];
	spellsCost[12] = spellsCost[cur];
	
	var const : float = 0;
	switch (aux[1].Count)
	{
		case 2:
			const = 30;
			break;
		case 1:
			const = 60;
		case 0:
			return;
	}
	
	for (var i = 0 ; i < nextLvls.Count; i++)
	{
		var array = ((aux[1] as Array)[i] as System.Object[]);
		spellsPos[13+i].x = itemsX + const + i * 40;
		spellsTextures[13+i] = array[0];
		spellsName[13+i] = array[1];
		spellsCost[13+i] = array[2];
		spellsDiscription[12+i] = array[3];
	}
}


function Update ()
{
	var mx = Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
	
	
	var aux = spells.GetNormalizedCDs ();
	var hov = -1;
	var i : int;
	for (i = 0; i < 7; i ++)
	{
		cdPos[i].height = -71 * aux[i];
		if (spellsPos[i].Contains(mx))
			hov = i;
	}
	if (shopping)
	{
		for (i = 7; i < 12; i++)
		{
			if (spellsPos[i].Contains(mx))
			{
				hov = i;
				break;
			}
		}
		if (curSlot != -1)
		{
			for (i = 12; i < 13 + nextLvls.Count; i++)
			{
				if (spellsPos[i].Contains(mx))
				{
					hov = i;
					break;
				}
			}
		}
	}
	if (hov == hover)
		hoverTime += Time.deltaTime;
	else
		hover = hov;
	
	gotInput = false;
	if (Input.GetButtonDown ("Fire1"))
	{
		if (hover != -1)
		{
			gotInput = true;
			if (hover < 7)
				inputManager.GetSpellInput(hover);
			else if (hover < 12)
				SetItemSelectionTree(hover-7, -1,hover);
			else if (hover == 12 && curSlot != -1)
			{
				if (spells.UpgradeItem(curSlot, curLvl, spellsCost[12]))
					curSlot = -1;
			}
			else
				SetItemSelectionTree(curSlot, nextLvls[hover-13], hover);
		}
	}
	
	var temp = playerStats.GetHpInfo(); // hp, maxHp, hpRegem;
	currentHpPos.width = 582 * (temp.x / temp.y);
	hpText = parseInt(temp.x).ToString() + '/' + temp.y.ToString();
	if (temp.z >= 0)
		regenText = "+ " + temp.z.ToString();
	else
		regenText = "- " + temp.z.ToString();
		
	regenTextPos.width = myStyle.CalcSize(new GUIContent(regenText)).x;
	regenTextPos.x = Screen.width/2 + 253 - regenTextPos.width;
	hpTextPos.width = myStyle.CalcSize(new GUIContent(hpText)).x;
	hpTextPos.x = Screen.width/2 - 35 - hpTextPos.width/2;
		
	var mul = playerStats.Exp/PlayerManager.lvlUpExp;
	if (mul > 1)
		mul = 1;
	currentExpPos.height = mul * -154;
}

function OnGUI ()
{
	GUI.color = Color.white;
	GUI.DrawTexture(hud, hudTexture);
	var i : int;
	for (i = 0; i < 7; i ++)
	{
		GUI.DrawTexture(spellsPos[i], spellsTextures[i]);
		GUI.DrawTexture(cdPos[i], cdTexture);
	}
	GUI.DrawTexture(selectPos, selectBox);
	
	if (shopping)
	{
		for (i = 7; i < 12; i++)
			GUI.DrawTexture(spellsPos[i], spellsTextures[i]);
	}
	if (curSlot != -1)
	{
		for (i = 12; i < 13 + nextLvls.Count; i++)
			GUI.DrawTexture(spellsPos[i], spellsTextures[i]);			
	}
	
	GUI.DrawTexture(expBoxPos,expBoxTexture);
	GUI.DrawTexture(currentHpPos,currentHpTexture);
	GUI.DrawTexture(currentExpPos,currentExpTexture);
	
	for (i = 0; i < 10; i++)
	{
		if (currentHpPosHeroes[i].width != 0)
		{
			GUI.DrawTexture(hpBoxPosHeroes[i], hpBoxTexture);
			GUI.DrawTexture(currentHpPosHeroes[i], currentHpTexture);
		}
	}
	
	GUI.Label(hpTextPos, hpText);
	GUI.Label(regenTextPos, regenText);
	GUI.Label(lvlTextPos, playerStats.Lvl.ToString(), boldtext);
	GUI.color = Color.black;
	GUI.Label(expTextPos, parseInt(playerStats.Exp).ToString());
	GUI.Label(goldTextPos, parseInt(playerStats.Gold).ToString());
}

function get mainPlayerTeam ()
{
	if (playerStats)
		return playerStats.heroNumber;
	return -1;
}

function get usableMouseInput ()
{
	return !gotInput;
}

function set SafeGround (value : bool)
{
	safeGround = value;
}

function set Shopping (value : bool)
{
	shopping = value;
	curSlot = -1;
}