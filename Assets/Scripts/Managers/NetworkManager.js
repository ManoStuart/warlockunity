#pragma strict
#pragma implicit
#pragma downcast

var playerPrefab : GameObject;
var spawnPoint : Transform;
var gameName : String = "Stuart_test_warlock_game";

private var refreshing:boolean;
private var hostData:HostData[];

private var btnX : float;
private var btnY : float;
private var btnW : float;
private var btnH : float;

var spawnType : int;
var select : int = -1;

function Start () 
{
	select = -1;
	btnX = Screen.width * 0.05;
	btnY = Screen.height * 0.05;
	btnW = Screen.width * 0.1;
	btnH = Screen.width * 0.1;
}

function StartServer ()
{
	Network.InitializeServer(20, 25001, !Network.HavePublicAddress);
	MasterServer.RegisterHost(gameName, "Warlock Mah nigga", "Warlock FTW");
}

function RefreshHostList()
{
	MasterServer.RequestHostList(gameName);
	refreshing = true;
}

function Update ()
{
	if (refreshing)
	{
		if (MasterServer.PollHostList().Length > 0)
		{
			refreshing = false;
			hostData = MasterServer.PollHostList();
		}
	}
}

function SpawnPlayer (layer:int, name : String)
{
	var player : GameObject = Network.Instantiate(playerPrefab, spawnPoint.position, Quaternion.identity, 0);
	player.networkView.RPC("InitializePlayer", RPCMode.AllBuffered, layer, name);
	Camera.main.GetComponent(CameraControl).target = player.transform;
	GUIManager.gui.SetPlayer(player);
}

function OnServerInitialized()
{
	SpawnPlayer(0, "Bolar King");
}

function OnConnectedToServer ()
{
	SpawnPlayer (spawnType * 5 + 1, "Judas");
}

// GUI
function OnGUI ()
{
	var i : int;
	var aux = ["Bro", "Hoe"];
	if (!Network.isClient && !Network.isServer)
	{
		if (GUI.Button(Rect(btnX,btnY,btnW,btnH), "Start Server"))
			StartServer();
		
		if (GUI.Button(Rect(btnX, btnY * 1.2 + btnH, btnW, btnH), "Refresh Hosts"))
		{
			select = -1;
			RefreshHostList();
		}
			
		if (hostData)
		{
			if (select == -1)
			{
				for (i = 0; i < hostData.length; i++)
				{
					if (GUI.Button(Rect(btnX *1.5 + btnW, btnY *1.2 + (btnH * i), btnW * 3, btnH * 0.5), hostData[i].gameName))
						select = i;
				}
			}
			else
			{
				for (i = 0; i < 2; i++)
				{
					if (GUI.Button(Rect(btnX *1.5 + btnW, btnY *1.2 + (btnH * i), btnW * 3, btnH * 0.5), aux[i]))
					{
						spawnType = i;
						Network.Connect(hostData[select]);
					}
				}
			}
		}
	}
}

static function GetNetworkView (name : String, obj : GameObject) : NetworkView
{
	for (var comp : NetworkView in obj.GetComponents(NetworkView))
	{
		if (comp.observed == name)
			return comp;
	}

	return null;
}