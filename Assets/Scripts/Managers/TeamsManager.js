#pragma strict
static var teams : TeamsManager;

var players : PlayerManager[] = new PlayerManager[10];

function Start ()
{
	TeamsManager.teams = this;
	enabled = false;
}

function HeroKill (killer : int, exp : float, gold : float)
{
	var team : int = 0;
	if (killer > 4)
		team = 5;
	
	for (var i : int = team; i < 5 + team; i++)
	{
		if (!players[i])
			continue;
		
		if (i == killer)
			players[i].networkView.RPC("HeroKill", RPCMode.All, exp * 0.3, gold * 0.6);
		else
		{
			if (players[i].dead)
				players[i].networkView.RPC("HeroKill", RPCMode.All, exp * 0.1, gold * 0.1);
			else
				players[i].networkView.RPC("HeroKill", RPCMode.All, exp * 0.2, gold * 0.15);
		}
	}
}

static function GetOtherTeam (layer : int) : PlayerManager[]
{
	if (layer >= 12)
		return teams.players[:5];
	else
		return teams.players[5:];
}

static function GetTeam (layer : int) : PlayerManager[]
{
	if (layer >= 12)
		return teams.players[5:];
	else
		return teams.players[:5];
}