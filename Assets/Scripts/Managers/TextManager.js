﻿#pragma strict
#pragma implicit
#pragma downcast

private var myStyle : GUIStyle = new GUIStyle();
private var logWidth : float;
var logFadeTime : float;
private var log : Array;

static var instance : TextManager;

function Start () {
	instance = this;
	log = new Array();
	logWidth = Screen.width / 3;
	myStyle.alignment = TextAnchor.MiddleCenter;
}

function Update () {
	var aux : float;
	for (var i = log.Count -1; i >= 0; i--)
	{
		aux = (log[i] as System.Object[])[1];
		if (aux < 0)
			break;
	
		(log[i] as System.Object[])[1] = aux - Time.deltaTime;
	}
}

@RPC
function AddLog(str : String)
{
	var array = FitString(str, logWidth);
	for (var a : String in array)
		log.Add([a, logFadeTime]);
}

function OnGUI () {

	GUI.BeginGroup (new Rect (10, Screen.height - 700, logWidth, 540));
	var pos = 520;
	var aux : float;
	for (var i = log.Count -1; i >= 0; i--)
	{
		aux = (log[i] as System.Object[])[1];
		if (aux < 0)
			break;

		GUI.Label(Rect(0,pos, logWidth, 20), (log[i] as System.Object[])[0] as String);
		pos -= 20;
	}
	GUI.EndGroup ();
}

function FitString (str : String, width : float) : Array
{
	var res : Array = new Array();
	var size : float = myStyle.CalcSize(new GUIContent(str)).x;
	
	while (size > width)
	{
		var i = (width / size) * str.Length;

		while (str[i] != ' ')
			i++;
		if (myStyle.CalcSize(new GUIContent(str[:i])).x > width)
		{
			i--;
			while(str[i] != ' ')
				i--;
		}
		res.Add(str[:i]);
		str = str[++i:];
		size = myStyle.CalcSize(new GUIContent(str)).x;
	}
	res.Add(str);
	
	return res;
}