#pragma strict
var delta = 15;
var mSpeed = 40.0;
var target : Transform;
var height : int = 15;
var hit : RaycastHit;
private var mask : LayerMask = 1 << 16;

function Update ()
{
	var aux : Vector3 = transform.position;
	aux.z += 5;
	var ree = Ray(aux, Vector3(0,-1,0));
	Physics.Raycast(ree, hit, 1000, mask.value);
	transform.position.y = hit.point.y + height;

	var x = Input.mousePosition.x;
	var y = Input.mousePosition.y;
	
    if ( x >= Screen.width - delta )
    {
        transform.position += Vector3.right * Time.deltaTime * mSpeed;
    }
    
    if ( x <= delta )
    {
        transform.position += Vector3.right * Time.deltaTime * mSpeed * -1;
    }
    
    if ( y >= Screen.height - delta )
    {
        transform.position += Vector3.forward * Time.deltaTime * mSpeed;
    }
    
    if ( y <= delta )
    {
        transform.position += Vector3.forward * Time.deltaTime * mSpeed * -1;
    }
    
    if (Input.GetButtonDown ("CameraToPlayer") && target != null)
    {
    	transform.position = target.position;
    	transform.position -= transform.rotation * Vector3.forward * 5;
    	transform.position.y = height;
    }
    
    if (Input.GetAxis("Mouse ScrollWheel") < 0 && height < 25)
    {
    	height++;
    		
    	transform.position.y = height;
    	transform.LookAt(hit.point);
    	GUIManager.gui.SetHpLenght(750/height, 105 / height);
    }
    else if (Input.GetAxis("Mouse ScrollWheel") > 0 && height > 4)
    {
    	height--;
    		
    	transform.position.y = height;
    	transform.LookAt(hit.point);
    	GUIManager.gui.SetHpLenght(750/height, 105 / height);
    }
}