﻿#pragma strict
#pragma implicit
#pragma downcast

private var pool = {};

public static var instance : SpellPool;

function Awake ()
{
	SpellPool.instance = this;
	enabled = false;
}

function GetObj (name : String) : GameObject
{
	if (!pool[name])
		pool.Add(name, new Array());

	var array : Array = pool[name];
	var aux : GameObject;

	if (array.Count == 0)
	{
		Debug.Log("added "+name);
		var prefab = Resources.Load(name) as GameObject;
		aux = Network.Instantiate(prefab, Vector3(-1000, -1000, -1000), Quaternion.identity, 0);
	}
	else
		aux = array.Pop();
		
	return aux;
}

function ReturnObj (name : String, spell : GameObject)
{
	(pool[name] as Array).Add(spell);
}