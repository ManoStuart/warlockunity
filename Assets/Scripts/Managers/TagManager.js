﻿#pragma strict
#pragma implicit
#pragma downcast

public class TagManager extends UnitManager
{
	public enum tagName {Default = 0, TransparentFX = 1, IgnoreRaycast = 2, a = 3, Water = 4, b = 5, c = 6, d = 7, Bro = 8,
		BroProjectiles = 9, BroSpells = 10, BroBuildings = 11, Hoe = 12, HoeProjectiles = 13, HoeSpells = 14,
		HoeBuildings = 15, Ground = 16, Neutral = 17};

	public static function GetLayerMask (array : tagName[]) : int
	{
		var mask = 0;
		for (var layer = 0; layer < array.length; layer++)
		{
			var num : int = array[layer];
			mask += 1 << num;
		}

		return mask;
	}

	public static function CheckIfSpell (layer : int) : boolean
	{
		switch(layer)
		{
			case TagManager.tagName.BroProjectiles:
			case TagManager.tagName.HoeProjectiles:
				return true;
		}
		return false;
	}

	public static function CheckIfPlayer (layer : int) : boolean
	{
		switch(layer)
		{
			case TagManager.tagName.Bro:
			case TagManager.tagName.Hoe:
				return true;
		}
		return false;
	}

	public static function CheckIfBuilding (layer : int) : boolean
	{
		switch(layer)
		{
			case TagManager.tagName.BroBuildings:
			case TagManager.tagName.HoeBuildings:
				return true;
		}
		return false;
	}
}