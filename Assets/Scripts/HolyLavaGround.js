﻿#pragma strict
private var deltas : float[] = new float[10];

function OnTriggerEnter (other : Collider)
{
	if (other.gameObject.name == "Character(Clone)" && other.networkView.isMine)
	{
		var player = other.gameObject.GetComponent(PlayerManager);
		var delta = -0.1 * player.maxH;
		if (delta > -25)
			delta = -25;
		deltas[player.heroNumber] = delta;
		player.ApplyDelta(PlayerManager.stat.hpRegem, delta, 0);
		player.ChangeGoldGain(10);
		player.ChangeUnreliableGain(-0.06);
	}
}

function OnTriggerExit (other : Collider)
{
	if (other.gameObject.name == "Character(Clone)" && other.networkView.isMine)
	{
		var player = other.gameObject.GetComponent(PlayerManager);		
		player.ApplyDelta(PlayerManager.stat.hpRegem, -deltas[player.heroNumber], 0);
		player.ChangeGoldGain(-10);
		player.ChangeUnreliableGain(0.06);
	}
}