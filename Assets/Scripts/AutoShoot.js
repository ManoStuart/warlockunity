#pragma strict

var projectile : GameObject;
var atkModifiers : Vector3;
var atkPower : float = 10;

function Start () {
	Invoke ("shoot", 5);
}

function shoot ()
{
	var clone : GameObject = SpellPool.instance.GetObj ("spellsPrefabs/Fireball");
 		clone.transform.position = transform.position;
 		clone.transform.rotation = transform.rotation;
 		clone.networkView.RPC(	"SetProjectile",
	 							RPCMode.AllBuffered,
	 							13,
	 							transform.forward,
	 							Vector3.one * atkPower, 
	 							atkModifiers,
	 							NetworkViewID.unassigned,
	 							1);
	Invoke ("shoot", 2);
}