#pragma strict
#pragma implicit
#pragma downcast

public class Chest extends Item
{
	// ITEM TREE:
	//			Shoulder	armor 3
	//			Guard 0		++ armor, -movSpd 4
	//			+armor		
	//
	//						Wex	5		Aura armor 8
	//	null 	Cape 1		Quas 6		neg Aura armor 9
	//			armor		exort 7		neg AtkSpd 10
	//
	//									Wex 12
	//			Wings 2		stats 11	Exort 13	aganin 15
	//			++Stats					Quas 14
	
	function Awake ()
	{
		super.Awake();
		nextLvl = new Array (0,1,2);
		texture = Resources.Load("textures/hud/items/Chest") as Texture;
		itemName = "Chest";
		discription = "shit to hold in da hand";
		
		lvlDependency = new Function[16];
		lvlDependency[0] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "Staff";
			var g = 5;
			var d = "Piece of wood to hit stuff.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(3,4);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.armor, 5, true);
			nextLvl.Add(3); nextLvl.Add(4);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			MeshManager.instance.networkView.RPC("AddMesh", RPCMode.AllBuffered, "chest_ck", pos, false);
			return;
		};
		
		lvlDependency[3] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "Desolator";
			var g = 5;
			var d = "Wreck dem armors";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.armor, 3, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			return;
		};
		
		lvlDependency[4] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "Basher";
			var g = 100;
			var d = "Pure skill.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.armor, 10, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.movementSpeed, -2, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[1] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "Gloves";
			var g = 40;
			var d = "Leather yo.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(5,6,7);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.armor, 3, true);
			nextLvl.Add(5); nextLvl.Add(6); nextLvl.Add(7);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			MeshManager.instance.networkView.RPC("AddMesh", RPCMode.AllBuffered, "chest_pa", pos, true);
			return;
		};
		
		lvlDependency[5] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "Gloves of dmg";
			var g = 80;
			var d = "dmg";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(8,9,10);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.quas, 5, true);
			nextLvl.Add(8); nextLvl.Add(9); nextLvl.Add(10);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[6] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "Gloves of dmg";
			var g = 80;
			var d = "dmg";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(8,9,10);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.wex, 5, true);
			nextLvl.Add(8); nextLvl.Add(9); nextLvl.Add(10);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[7] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "Gloves of dmg";
			var g = 80;
			var d = "dmg";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(8,9,10);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.exort, 5, true);
			nextLvl.Add(8); nextLvl.Add(9); nextLvl.Add(10);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[8] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "Gloves of dmg";
			var g = 80;
			var d = "dmg";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();

			InstantiateAura (PlayerManager.effect.stackable, "ItemArmor", 3, PlayerManager.stat.armor, 4);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[9] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "Gloves of dmg";
			var g = 80;
			var d = "dmg";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			InstantiateAura (PlayerManager.effect.stackable, "ItemNegArmor", -3, PlayerManager.stat.armor, 5);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[10] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "Gloves of dmg";
			var g = 80;
			var d = "dmg";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			InstantiateAura (PlayerManager.effect.stackable, "ItemNegAtkSpd", -0.5, PlayerManager.stat.atkSpeed, 3);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[2] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "Wand";
			var g = 20;
			var d = "Same as Harry's";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
			{
				var aux = new Array();
				aux.Add(11);
				return aux;
			}
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.quas, 10, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.wex, 10, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.exort, 10, true);
			nextLvl.Add(11);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			MeshManager.instance.networkView.RPC("AddMesh", RPCMode.AllBuffered, "chest_qop", pos, true);
			return;
		};
		
		lvlDependency[11] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "Hyper";
			var g = 5;
			var d = "nothing but a blur.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(12,13,14);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.quas, 5, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.wex, 5, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.exort, 5, true);
			nextLvl.Add(12); nextLvl.Add(13); nextLvl.Add(14);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[12] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "medio";
			var g = 5;
			var d = "med";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
			{
				var aux = new Array();
				aux.Add(15);
				return aux;
			}
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.quas, 5, true);
			nextLvl.Add(15);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[13] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "medio";
			var g = 5;
			var d = "med";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
			{
				var aux = new Array();
				aux.Add(15);
				return aux;
			}
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.wex, 5, true);
			nextLvl.Add(15);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[14] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "medio";
			var g = 5;
			var d = "med";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
			{
				var aux = new Array();
				aux.Add(15);
				return aux;
			}
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.exort, 5, true);
			nextLvl.Add(15);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[15] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Chest") as Texture;
			var n = "medio";
			var g = 5;
			var d = "med";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			// Aganin Shit
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
	}
}