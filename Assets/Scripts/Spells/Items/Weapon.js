﻿#pragma strict
#pragma implicit
#pragma downcast

public class Weapon extends Item
{
	// ITEM TREE:
	//						Armor Reduc 3
	//			staff 0		Bash 4
	//			+Stats		Slow 5
	//
	//						++ dmg 6
	//	null 	gloves 1	Crit + dmg 7
	//			+dmg		miniBash + dmg 8
	//
	//						++ atkSpd 9
	//			wand 2		atkSpd/atkRang 10
	//			+atkSpd		++ atkRang 11
	
	function Awake ()
	{
		super.Awake();
		nextLvl = new Array (0,1,2);
		texture = Resources.Load("textures/hud/items/Weapon") as Texture;
		itemName = "Weapon";
		discription = "shit to hold in da hand";
		
		lvlDependency = new Function[12];
		lvlDependency[0] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Staff") as Texture;
			var n = "Staff";
			var g = 5;
			var d = "Piece of wood to hit stuff.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(3,4,5);
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.quas, 5, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.wex, 5, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.exort, 5, true);
			nextLvl.Add(3); nextLvl.Add(4); nextLvl.Add(5);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			MeshManager.instance.networkView.RPC("AddMesh", RPCMode.AllBuffered, "weapon_rubick", pos, false);
			AnimatorNetworkInterface.SwapController("CharacterController_S");
			return;
		};
		
		lvlDependency[3] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Weapon") as Texture;
			var n = "Desolator";
			var g = 5;
			var d = "Wreck dem armors";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			var procSp = gameObject.AddComponent(ProcSpellCast);
			procSp.SetVariables (0, 4, -3, 5);
			procs.Add(procSp);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			return;
		};
		
		lvlDependency[4] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Weapon") as Texture;
			var n = "Basher";
			var g = 100;
			var d = "Pure skill.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			var procSp = gameObject.AddComponent(ProcSpellCast);
			procSp.SetVariables (0.4, 2, 0, 0);
			procs.Add(procSp);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[5] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Weapon") as Texture;
			var n = "Skadi";
			var g = 100;
			var d = "The eye.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			var procSp = gameObject.AddComponent(ProcSpellCast);
			procSp.SetVariables (0, 4, -2, 3);
			procs.Add(procSp);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[1] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Weapon") as Texture;
			var n = "Gloves";
			var g = 40;
			var d = "Leather yo.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(6,7,8);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.damage, 5, true);
			nextLvl.Add(6); nextLvl.Add(7); nextLvl.Add(8);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[6] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Weapon") as Texture;
			var n = "Gloves of dmg";
			var g = 80;
			var d = "dmg";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.damage, 40, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[7] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Weapon") as Texture;
			var n = "Daedalus";
			var g = 80;
			var d = "Critlord.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			var procSp = gameObject.AddComponent(CritProc);
			procSp.SetVariables (0.5, 2.5, 0);
			procs.Add(procSp);
			//PlayerManager.instance.SetBaseVariable (PlayerManager.stat.damage, 10, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[8] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Weapon") as Texture;
			var n = "MiniStun";
			var g = 80;
			var d = "ministun galore";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			var procSp = gameObject.AddComponent(CritProc);
			procSp.SetVariables (0.5, 0.1, 0, 0);
			procSp.SetVariables (0.5, 1, 50);
			procs.Add(procSp);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.damage, 20, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[2] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Weapon") as Texture;
			var n = "Wand";
			var g = 5;
			var d = "Same as Harry's";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(9,10,11);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.atkSpeed, 0.6, true);
			nextLvl.Add(9); nextLvl.Add(10); nextLvl.Add(11);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			MeshManager.instance.networkView.RPC("AddMesh", RPCMode.AllBuffered, "weapon_skywr", pos, false);
			//AnimatorNetworkInterface.SwapController("CharacterController_W");
			return;
		};
		
		lvlDependency[9] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Weapon") as Texture;
			var n = "Hyper";
			var g = 60;
			var d = "nothing but a blur.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.atkSpeed, 1, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[10] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Weapon") as Texture;
			var n = "medio";
			var g = 60;
			var d = "med";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.atkSpeed, 0.3, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.atkRange, 0.2, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[11] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Weapon") as Texture;
			var n = "range";
			var g = 60;
			var d = "range";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.atkRange, 0.5, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
	}
}