#pragma strict
#pragma implicit
#pragma downcast

public class Feet extends Item
{
	// ITEM TREE:
	//						movSpd 3
	//			Shoes 0		++MovSpd when noDmg 4
	//			+movSpd		
	//
	//						
	//	null 	Slippers 1	Aura MovSpd 5	Stats 6
	//			movSpd		
	//
	//						NoPush proc 7
	//			Boot 2		KnockbackReduc 8	Armor 9
	//			-movSpd		
	
	function Awake ()
	{
		super.Awake();
		nextLvl = new Array (0,1,2);
		texture = Resources.Load("textures/hud/items/Feet") as Texture;
		itemName = "Weapon";
		discription = "shit to hold in da hand";

		lvlDependency = new Function[10];
		lvlDependency[0] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Feet") as Texture;
			var n = "Shoes";
			var g = 5;
			var d = "Piece of wood to hit stuff.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(3,4);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.movementSpeed, 1.5, true);
			nextLvl.Add(3); nextLvl.Add(4);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			MeshManager.instance.networkView.RPC("AddMesh", RPCMode.AllBuffered, "feet_kunkka", pos, true);
			return;
		};
		
		lvlDependency[3] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Feet") as Texture;
			var n = "Desolator";
			var g = 5;
			var d = "Wreck dem armors";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.movementSpeed, 0.5, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[4] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Feet") as Texture;
			var n = "Basher";
			var g = 5;
			var d = "Pure skill.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.movementSpeed, 1, true);
			procs.Add(gameObject.AddComponent(ReduceStatProc));
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[1] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Feet") as Texture;
			var n = "Gloves";
			var g = 40;
			var d = "Leather yo.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
			{
				var aux = new Array();
				aux.Add(5);
				return aux;
			}
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.movementSpeed, 1, true);
			nextLvl.Add(5);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[5] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Feet") as Texture;
			var n = "Gloves of dmg";
			var g = 80;
			var d = "dmg";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
			{
				var aux = new Array();
				aux.Add(6);
				return aux;
			}

			InstantiateAura(PlayerManager.effect.stackable, "ItemMovSpd", 0.5, PlayerManager.stat.movementSpeed, 4);
			nextLvl.Add(6);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[6] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Feet") as Texture;
			var n = "Daedalus";
			var g = 80;
			var d = "Critlord.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.quas, 5, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.wex, 5, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.exort, 5, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[2] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Feet") as Texture;
			var n = "Wand";
			var g = 5;
			var d = "Same as Harry's";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(7,8);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.movementSpeed, 0.5, true);
			nextLvl.Add(7); nextLvl.Add(8);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			MeshManager.instance.networkView.RPC("AddMesh", RPCMode.AllBuffered, "feet_dk", pos, false);
			return;
		};
		
		lvlDependency[7] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Feet") as Texture;
			var n = "Hyper";
			var g = 5;
			var d = "nothing but a blur.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			procs.Add(gameObject.AddComponent(NoPushProc));
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[8] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Feet") as Texture;
			var n = "medio";
			var g = 60;
			var d = "med";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
			{
				var aux = new Array();
				aux.Add(9);
				return aux;
			}
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.knockbackReduction, 0.4, true);
			nextLvl.Add(9);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[9] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Feet") as Texture;
			var n = "range";
			var g = 60;
			var d = "range";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.armor, 5, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
	}
}