#pragma strict
#pragma implicit
#pragma downcast

public class Item extends MonoBehaviour
{
	protected var procs : Array = new Array(); // lembrar q pode ser dos dois tipos;
	protected var auras : Array = new Array();
	protected var model : String;
	protected var modelPath : String;
	protected var texture : Texture;
	protected var itemName : String;
	protected var discription : String;
	protected var gold : int;
	protected var lvlDependency : Function[]; // 0 - lvlUp, 1 - returns texture, name, gold, and discription, 2 - returns nextLvls;
	protected var lvl : int;
	protected var nextLvl : Array = new Array();
	var pos : int;
	
	function Awake ()
	{
		// Initial Setup -- LvlDependency, nextLvl, texture, modelpath, model, lvlDependency Size;
		lvl = -1;
	}
	
	function IsNextLvl (num : int) : boolean
	{
		var bool = false;
		for (var i : int in nextLvl)
			if (i == num)
			{
				bool = true;
				break;
			}
		
		return bool;
	}
	
	function SetLvl (num : int) : boolean
	{
		nextLvl.clear();
		lvl = num;
		(lvlDependency[num] as Function)(0);
		GUIManager.gui.SetItem(pos, CurItem);
		// SetModel
		return true;
	}
	
	function get CurItem () : System.Object[]
	{
		return [texture, itemName, gold, discription];
	}
	
	function PossibleTree (num : int) : Array[]
	{
		var aux : Array;
		if (num == -1)
			aux = nextLvl;
		else
			aux = (lvlDependency[num] as Function)(2);
			
		var aux2 : Array = new Array();
		for (var i : int in aux)
			aux2.Add((lvlDependency[i] as Function)(1));
			
		return [aux, aux2];
	}
}

function InstantiateAura (effect : int, str : String, ext : float, num : int, array : int) : AuraSpell
{
	var viewID = Network.AllocateViewID();
	AuraManager.instance.networkView.RPC("InstantiateAuraSpell", RPCMode.AllBuffered, viewID, effect, str, ext, num, array);
	var aura = NetworkView.Find(viewID).gameObject.GetComponent(AuraSpell);
	auras.Add(aura);

	return aura;
}