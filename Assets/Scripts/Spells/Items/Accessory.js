#pragma strict
#pragma implicit
#pragma downcast

public class Accessory extends Item
{
	// ITEM TREE:
	//						Full Block, -armor 3
	//			shield 0	+ Knockreduc 4
	//			+armor		+ armor 5
	//
	//						+Quas 6
	//	null 	armlet 1	+Wex 7		Purge 9
	//			stats		+Exort 8
	//
	//						Quas 10		Dmg 13		lifeSteal 16
	//			rings 2		Wex 11		atkSpd 14	Hp 17			Gold 19
	//			-- Stats	Exort 12	movSpd 15	Hp Regen 18		Aura 20
	
	var rings : int[] = new int[3];
	var ringsExt : float[] = new float[3];
	
	function Awake ()
	{
		super.Awake();
		nextLvl = new Array (0,1,2);
		texture = Resources.Load("textures/hud/items/Accessory") as Texture;
		itemName = "Accessory";
		discription = "shit to hold in da hand";
		
		lvlDependency = new Function[21];
		lvlDependency[0] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "Staff";
			var g = 5;
			var d = "Piece of wood to hit stuff.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(3,4,5);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.armor, 5, true);
			nextLvl.Add(3); nextLvl.Add(4); nextLvl.Add(5);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			MeshManager.instance.networkView.RPC("AddMesh", RPCMode.AllBuffered, "accessory_ck", pos, false);
			return;
		};
		
		lvlDependency[3] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "Desolator";
			var g = 5;
			var d = "Wreck dem armors";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.armor, -5, true);
			// FullBlock Shit
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			return;
		};
		
		lvlDependency[4] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "Basher";
			var g = 100;
			var d = "Pure skill.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.armor, 5, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[5] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "Skadi";
			var g = 100;
			var d = "The eye.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.knockbackReduction, 0.3, true);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[1] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "Gloves";
			var g = 40;
			var d = "Leather yo.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(6,7,8);
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.quas, 5, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.wex, 5, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.exort, 5, true);
			nextLvl.Add(6); nextLvl.Add(7); nextLvl.Add(8);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			MeshManager.instance.networkView.RPC("AddMesh", RPCMode.AllBuffered, "accessory_pa", pos, false);
			return;
		};
		
		lvlDependency[6] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "Gloves of dmg";
			var g = 80;
			var d = "dmg";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
			{
				var aux = new Array();
				aux.Add(9);
				return aux;
			}
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.quas, 5, true);
			nextLvl.Add(9);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[7] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "Daedalus";
			var g = 80;
			var d = "Critlord.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
			{
				var aux = new Array();
				aux.Add(9);
				return aux;
			}
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.wex, 5, true);
			nextLvl.Add(9);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[8] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "MiniStun";
			var g = 80;
			var d = "ministun galore";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
			{
				var aux = new Array();
				aux.Add(9);
				return aux;
			}
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.exort, 5, true);
			nextLvl.Add(9);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[9] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "MiniStun";
			var g = 80;
			var d = "ministun galore";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			procs.Add(gameObject.AddComponent(PurgeProc));
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[2] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "Wand";
			var g = 5;
			var d = "Same as Harry's";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(10,11,12);
			
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.quas, 2, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.wex, 2, true);
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.exort, 2, true);
			nextLvl.Add(10); nextLvl.Add(11); nextLvl.Add(12);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[10] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "Hyper";
			var g = 5;
			var d = "nothing but a blur.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(13,14,15);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.quas, 3, true);
			nextLvl.Add(13); nextLvl.Add(14); nextLvl.Add(15);
			rings[0] = PlayerManager.stat.quas;
			ringsExt[0] = 2;
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[11] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "medio";
			var g = 5;
			var d = "med";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(13,14,15);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.wex, 3, true);
			nextLvl.Add(13); nextLvl.Add(14); nextLvl.Add(15);
			rings[0] = PlayerManager.stat.wex;
			ringsExt[0] = 2;
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[12] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "range";
			var g = 5;
			var d = "range";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(13,14,15);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.exort, 3, true);
			nextLvl.Add(13); nextLvl.Add(14); nextLvl.Add(15);
			rings[0] = PlayerManager.stat.exort;
			ringsExt[0] = 2;
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
			
		lvlDependency[13] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "Hyper";
			var g = 5;
			var d = "nothing but a blur.";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(16,17,18);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.damage, 5, true);
			nextLvl.Add(16); nextLvl.Add(17); nextLvl.Add(18);
			rings[1] = PlayerManager.stat.damage;
			ringsExt[1] = 5;
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[14] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "medio";
			var g = 5;
			var d = "med";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(16,17,18);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.atkSpeed, 0.3, true);
			nextLvl.Add(16); nextLvl.Add(17); nextLvl.Add(18);
			rings[1] = PlayerManager.stat.atkSpeed;
			ringsExt[1] = 0.2;
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[15] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "range";
			var g = 5;
			var d = "range";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(16,17,18);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.movementSpeed, 0.3, true);
			nextLvl.Add(16); nextLvl.Add(17); nextLvl.Add(18);
			rings[1] = PlayerManager.stat.movementSpeed;
			ringsExt[1] = 0.2;
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[16] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "range";
			var g = 5;
			var d = "range";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(19,20);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.lifeSteal, 0.1, true);
			nextLvl.Add(19); nextLvl.Add(20);
			rings[2] = PlayerManager.stat.lifeSteal;
			ringsExt[2] = 0.1;
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[17] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "range";
			var g = 5;
			var d = "range";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(19,20);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.maxHp, 50, true);
			nextLvl.Add(19); nextLvl.Add(20);
			rings[2] = PlayerManager.stat.maxHp;
			ringsExt[2] = 50;
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[18] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "range";
			var g = 5;
			var d = "range";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array(19,20);
				
			PlayerManager.instance.SetBaseVariable (PlayerManager.stat.hpRegem, 0.2, true);
			nextLvl.Add(19); nextLvl.Add(20);
			rings[2] = PlayerManager.stat.hpRegem;
			ringsExt[2] = 0.2;
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[19] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "range";
			var g = 5;
			var d = "range";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			PlayerManager.instance.ChangeGoldGain(0.2);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
		
		lvlDependency[20] = function (num : int) {
			var t = Resources.Load("textures/hud/items/Accessory") as Texture;
			var n = "range";
			var g = 5;
			var d = "range";
			
			if (num == 1)
				return [t,n,g,d];
			else if (num == 2)
				return new Array();
			
			var ind : int = PlayerManager.effect.stackable;
			var aura : AuraSpell = InstantiateAura (ind, "ItemRing", ringsExt[0], rings[0], 4, ringsExt[1], rings[1], ringsExt[2], rings[2]);
			texture = t;
			itemName = n;
			gold = g;
			discription = d;
			// model
			return;
		};
	}
}

function InstantiateAura (effect : int, str : String, ext : float, num : int, array : int,
							ext1 : float, num1 : int, ext2 : float, num2 : int) : AuraSpell
{
	var viewID = Network.AllocateViewID();
	AuraManager.instance.networkView.RPC("InstantiateAuraSpell2", RPCMode.AllBuffered, viewID, effect, str, ext, num, array, ext1, num1, ext2, num2);
	var aura = NetworkView.Find(viewID).gameObject.GetComponent(AuraSpell);
	auras.Add(aura);

	return aura;
}