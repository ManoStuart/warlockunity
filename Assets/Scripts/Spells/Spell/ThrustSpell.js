#pragma strict
#pragma implicit
#pragma downcast

public class ThrustSpell extends AnimatedSpell
{
	private var radius : float = 3.5;
	private var curCd : float = 0;
	private var targetLayer : int = TagManager.tagName.Bro;
	private var cooldown : float = 0.3;
	private var hited : boolean;
	private var fadeSpd : float;

	function Awake ()
	{
		baseDeltaHealthCons = -5;
		pushVelocity = 20;
		experienceGain = new Vector3 (10, 0, 0);
		this.enabled = false;
		fadeSpd = 1 / 1;
		renderer.enabled = false;
	}

	@RPC
	function SetOwner (player : NetworkViewID)
	{
		owner = player;
		var aux = NetworkView.Find(player);
		if (aux.gameObject.layer == TagManager.tagName.Bro)
			targetLayer = TagManager.GetLayerMask([TagManager.tagName.Hoe]);
		else
			targetLayer = TagManager.GetLayerMask([TagManager.tagName.Bro]);

		transform.parent = aux.transform;
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;
		gameObject.layer = aux.gameObject.layer + 2;
		curCd = cooldown;
	}

	@RPC
	function SetSpell (playerStats : Vector3, atkModifiers : Vector3, dmg : float,  player : NetworkViewID, lvlT : int)
	{
		super.SetSpell(playerStats, atkModifiers, dmg, player, lvlT);
		curCd = 0;
		enabled = true;
		hited = false;
		renderer.enabled = false;
	}

	@RPC
	function Set ()
	{
		renderer.material.color.a = 1;
		renderer.enabled = true;
		enabled = true;
		hited = true;
	}

	function Update ()
	{
		if (hited)
		{
			renderer.material.color.a -= fadeSpd * Time.deltaTime;
			if (renderer.material.color.a < 0)
			{
				renderer.enabled = false;
				enabled = false;
			}
		}
		else if (Network.isServer)
		{
			curCd += Time.deltaTime;
			if (curCd > cooldown)
				enabled = false;

			var colliders = Physics.OverlapSphere(transform.position, radius, targetLayer);
			for (var other : Collider in colliders)
			{
				if (other.gameObject.name.Contains("Character"))
				{
					other.gameObject.networkView.RPC("ApplySpell", RPCMode.AllBuffered, networkView.viewID, 
						(other.transform.position - transform.position).normalized, owner);
					networkView.RPC("Set", RPCMode.AllBuffered);
					enabled = false;
				}
			}
		}
		else
			enabled = false;
	}
}