#pragma strict
#pragma implicit
#pragma downcast

public class FireballProjectile extends ProjectileSpell
{
	private var flame : Transform;
	function Awake ()
	{
		flame = transform.FindChild("Flame");
		baseScale = 0.7;
		spellName = "spellsPrefabs/Fireball";
		sendDirection = true;
		if (!networkView.isMine)
			enabled = false;
	}

	function DeathAnimation ()
	{
		flame.parent = null;
		flame.FindChild("InnerCore").particleEmitter.emit = false;
		flame.FindChild("Lightsource").GetComponent(LightFade).enabled = true;
		flame.FindChild("OuterCore").particleEmitter.emit = false;
	}
	
	function Reset ()
	{
		flame.parent = transform;
		flame.transform.localPosition = Vector3.zero;
		flame.FindChild("InnerCore").particleEmitter.emit = true;
		flame.FindChild("Lightsource").light.range = 6.114449;
		flame.FindChild("OuterCore").particleEmitter.emit = true;
		
		super.Reset();
	}
	
	function ExtraSettings (atkModifiers : Vector3)
	{
		if (atkModifiers.z > 1)
			transform.localScale *= 1 + (atkModifiers.z - 1) / 4;
		else
			transform.localScale *= atkModifiers.z;
	}
}