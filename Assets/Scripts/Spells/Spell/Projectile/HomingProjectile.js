﻿#pragma strict
#pragma implicit
#pragma downcast

public class HomingProjectile extends ProjectileSpell
{
	private var target : Transform;
	private var start : Vector3;
	private var toStart : boolean;
	private var followSpeed = 5;
	var turnRate : float = 0.075;

	function Awake ()
	{
		spellName = "spellsPrefabs/Hoaming";
		sendDirection = false;
		baseSpeed = 15;
		baseRange = 30;
		baseDeltaHealthCons = -5;
		pushVelocity = 20;
		baseScale = 0.7;
		enabled = false;
		if (!networkView.isMine)
			enabled = false;
	}
	
	@RPC
	function SetProjectile (layer :int, direction :Vector3, playerStats :Vector3, atkModifiers :Vector3, dmg : float, player :NetworkViewID, lvl : int) // Quas, wex, Exort  ---  atkRange, projSpd, areaEffect
	{
		super.SetSpell (playerStats, atkModifiers, dmg, player, lvl);
		if (atkModifiers.z > 1)
			transform.localScale *= 1 + (atkModifiers.z - 1) / 4;
		else
			transform.localScale *= atkModifiers.z;

		gameObject.layer = layer;
		
		var array = TeamsManager.GetOtherTeam(layer);
		var min = -1;
		var mag : float = 1000000;
		for (var i = 0; i < 5; i++)
		{
			if (!array[i])
				continue;

			var aux = (array[i].transform.position - direction).magnitude;
			if (aux < mag)
			{
				mag = aux;
				min = i;
			}
		}
		if (min != -1)
			target = array[min].transform;
		else
			target = null;

		start = direction;
		toStart = true;
		followSpeed = baseSpeed * atkModifiers.y;

		var dir = direction - NetworkView.Find(player).transform.position;
		dir.y = 0;
		dir = dir.normalized;
		rigidbody.velocity = Vector3.zero;
		rigidbody.velocity = dir * followSpeed;
		lifeSeconds = (baseRange * atkModifiers.x) / followSpeed;
	}

	function Update ()
	{
		super.Update();
		if (target)
		{
			var aux = (target.position - transform.position).normalized;
			aux.y = 0;
			rigidbody.velocity = Vector3.RotateTowards(rigidbody.velocity.normalized,
					aux, turnRate,0) * followSpeed;
			transform.rotation = Quaternion.LookRotation(rigidbody.velocity);
		}
	}
}