#pragma strict
var distance = 2;

function Update () {
	var hit : RaycastHit;
    var mask : LayerMask = 1 << 16;
    var ree : Ray = Ray(transform.FindChild("front").position, Vector3(0,-1,0));
    if (Physics.Raycast(ree, hit, 1000, mask.value))
 	{
		transform.position.y = hit.point.y + distance;
 	}
}

function OnNetworkInstantiate (info : NetworkMessageInfo)
{
	if (!networkView.isMine)
		enabled = false;
}