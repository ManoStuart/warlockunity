﻿#pragma strict
#pragma implicit
#pragma downcast

public class ShurikenProjectile extends ProjectileSpell
{
	private var target : Transform;
	private var isPlayer : boolean;
	private var playerRadius : float;
	var turnRate : float = 1;
	var missReduc : float = 0.95;
	var acelleration : float = 5;
	var spinRate : float = 20;

	function Awake ()
	{
		spellName = "spellsPrefabs/Shuriken";
		sendDirection = true;
		baseSpeed = 1700;
		baseDeltaHealthCons = -5;
		pushVelocity = 20;
		enabled = false;
	}

	function Reset ()
	{
		super.Reset();
		isPlayer = false;
	}

	function Start ()
	{
		target = PlayerManager.instance.transform;
		playerRadius = (PlayerManager.instance.collider as CharacterController).radius;
	}

	function Update ()
	{
		var dist = target.position - transform.position;
		dist.y = 0;
		var mag = dist.magnitude;
		if (mag < transform.localScale.x * (collider as SphereCollider).radius + playerRadius)
		{
			networkView.RPC("Death", RPCMode.AllBuffered);
			enabled = false;
		}

		if (isPlayer)
			rigidbody.velocity += dist.normalized * acelleration;
		else
		{
			if (rigidbody.velocity.magnitude < 10)
			{
				rigidbody.velocity = Vector3.RotateTowards(rigidbody.velocity, dist, turnRate, 0);

				if (Vector3.Angle(rigidbody.velocity, dist.normalized) < 10)
				{
					rigidbody.velocity = dist.normalized * 6;
					isPlayer = true;
				}
			}
			else
			{
				rigidbody.velocity *= missReduc;
			}
		}

		transform.Rotate(Vector3(0, spinRate, 0));
	}

	function LateUpdate ()
	{
		if (isPlayer)
		{
			var dist = target.position - transform.position;
			if (Vector3.Angle(rigidbody.velocity, dist.normalized) > 60)
				isPlayer = false;
		}

	}

	function ExtraSettings (atkModifiers : Vector3)
	{
		if (atkModifiers.z > 1)
			transform.localScale *= 1 + (atkModifiers.z - 1) / 4;
		else
			transform.localScale *= atkModifiers.z;
	}
}