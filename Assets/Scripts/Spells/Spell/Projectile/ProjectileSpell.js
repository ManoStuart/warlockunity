#pragma strict
#pragma implicit
#pragma downcast

public class ProjectileSpell extends Spell
{
	var baseSpeed : float = 1000;
	var baseRange : float = 800;
	var lifeSeconds : float = 4;
	var baseScale : float = 1;
	var sendDirection : boolean = true;
	
	function Reset ()
	{
		super.Reset();
		transform.localScale = new Vector3(baseScale, baseScale, baseScale);
	}

	function Update () {
		lifeSeconds -= Time.deltaTime;
		if (lifeSeconds < 0)
		{
			networkView.RPC("Death", RPCMode.AllBuffered);
			enabled = false;
		}
	}
	
	@RPC
	function SetProjectile (layer :int, direction :Vector3, playerStats :Vector3, atkModifiers :Vector3, dmg : float, player :NetworkViewID, lvl : int) // Quas, wex, Exort  ---  atkRange, projSpd, areaEffect
	{
		SetSpell (playerStats, atkModifiers, dmg, player, lvl);
		
		gameObject.layer = layer;
		rigidbody.velocity = Vector3.zero;
		rigidbody.AddForce(direction * baseSpeed * atkModifiers.y);
		transform.rotation.LookRotation(direction);
		
		if (baseRange > 0)
			lifeSeconds = (baseRange * atkModifiers.x) / (baseSpeed * atkModifiers.y );
		else
			enabled = false;
		
		ExtraSettings (atkModifiers);
	}
	
	function ExtraSettings (atkModifiers : Vector3)
	{
	}

	function OnCollisionEnter(collision : Collision)
	{
		Debug.Log("collision " + collision.gameObject.name + ' ' + collision.gameObject.networkView.viewID);
		Debug.Log("current " + gameObject.name + ' ' + networkView.viewID);
		var expGain : float = ValidateApplySpell(collision.gameObject);
		if (expGain == -1)
			return;
			
		dead = true;
		
		collision.gameObject.networkView.RPC("ApplySpell",
											RPCMode.All,
											networkView.viewID,
											Vector3(-collision.contacts[0].normal.x, experienceGain.z, -collision.contacts[0].normal.z),
											owner);
		networkView.RPC("Hited", RPCMode.All, collision.gameObject.layer, expGain);
	}
}