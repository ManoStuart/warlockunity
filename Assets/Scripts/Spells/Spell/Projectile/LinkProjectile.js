﻿#pragma strict
#pragma implicit
#pragma downcast

public class LinkProjectile extends ProjectileSpell
{
	var link : NetworkView;
	var retreating : boolean = false;
	var minRange : float = 10;
	var pushForce : float = 500;
	
	function Awake ()
	{
		spellName = "spellsPrefabs/LinkProj";
		sendDirection = true;
		if (!networkView.isMine)
			enabled = false;
	}
	
	function Reset ()
	{
		super.Reset();
		retreating = false;
	}
	
	@RPC
	function SetProjectile (layer :int, direction :Vector3, playerStats :Vector3, atkModifiers :Vector3, dmg : float, player :NetworkViewID, lvl : int) // Quas, wex, Exort  ---  atkRange, projSpd, areaEffect
	{
		if (player != NetworkViewID.unassigned)
		{
			link = NetworkView.Find(player).transform.FindChild("Center/Link(Clone)").networkView;
			if (Network.isServer)
				link.RPC("Set", RPCMode.All, networkView.viewID, false); 
		}
		super.SetProjectile(layer, direction, playerStats, atkModifiers, dmg, player, lvl);
	}
	
	function ExtraSettings (atkModifiers : Vector3)
	{
		if (atkModifiers.z > 1)
			transform.localScale *= 1 + (atkModifiers.z - 1) / 4;
		else
			transform.localScale *= atkModifiers.z;
	}
	
	function Update ()
	{
		if (retreating)
		{
			var dist = PlayerManager.instance.transform.position - transform.position;
			dist.y = 0;
			if (dist.sqrMagnitude < minRange)
			{
				networkView.RPC("Death", RPCMode.AllBuffered);
				link.RPC("ShutDown", RPCMode.All, 0);
				enabled = false;
			}
			rigidbody.velocity = Vector3.zero;
			rigidbody.AddForce(dist.normalized * pushForce);
		}
		else
		{
			lifeSeconds -= Time.deltaTime;
			if (lifeSeconds < 0)
				retreating = true;
		}
	}
	
	function OnCollisionEnter(collision : Collision)
	{
		Debug.Log("collision " + collision.gameObject.name + ' ' + collision.gameObject.networkView.viewID);
		Debug.Log("current " + gameObject.name + ' ' + networkView.viewID);
		var expGain : float = ValidateApplySpell(collision.gameObject);
		if (expGain == -1)
			return;
			
		dead = true;
		
		link.RPC("Set", RPCMode.All, collision.gameObject.networkView.viewID, true);
		collision.gameObject.networkView.RPC("ApplySpell",
											RPCMode.All,
											networkView.viewID,
											Vector3(-collision.contacts[0].normal.x, experienceGain.z, -collision.contacts[0].normal.z),
											owner);
		networkView.RPC("Hited", RPCMode.All, collision.gameObject.layer, expGain);
	}
}