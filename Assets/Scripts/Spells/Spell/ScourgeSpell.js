﻿#pragma strict
#pragma implicit
#pragma downcast

public class ScourgeSpell extends AnimatedSpell
{
	private var range : AuraCheck;
	private var fade : float;
	private var curCd : float = 0;
	private var targetLayer : int = TagManager.tagName.Bro;
	private var cooldown : float = 0.3;

	function Awake ()
	{
		baseDeltaHealthCons = -5;
		pushVelocity = 20;
		experienceGain = new Vector3 (10, 10, 10);
		renderer.enabled = false;
		this.enabled = false;
		fade = 1 / cooldown;
	}

	@RPC
	function SetOwner (player : NetworkViewID)
	{
		super.SetOwner(player);
		var aux = NetworkView.Find(player);
		if (aux.gameObject.layer == TagManager.tagName.Bro)
			targetLayer = TagManager.tagName.Hoe;
		else
			targetLayer = TagManager.tagName.Bro;
		if (player != NetworkViewID.unassigned)
			range = aux.transform.Find("Aura10").GetComponent(AuraCheck);

		transform.parent = aux.transform;
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;
	}

	@RPC
	function SetSpellAnimation2 ()
	{
		fade = 1 / cooldown;
		renderer.material.color.a = 1;
		renderer.enabled = true;
		curCd = 0;
		enabled = true;
	}

	@RPC
	function CheckCast (target : Vector3, pos : Vector3, netID : NetworkViewID, lvl : int)
	{
		if (Network.isServer)
		{
			var player = NetworkView.Find(owner).GetComponent(PlayerManager);
			var array = range.GetArray(1);
			for (var enemy : GameObject in array)
				networkView.RPC("SetAndHit", RPCMode.AllBuffered, Vector3.zero, player.GetStats(), player.GetAtkModifiers(), player.dmg,
					lvl, enemy.networkView.viewID, (player.transform.position - enemy.transform.position).normalized, targetLayer, 0.0);
		
			networkView.RPC("SetSpellAnimation2", RPCMode.AllBuffered);
		}
	}

	function Update ()
	{
		curCd += Time.deltaTime;
		renderer.material.color.a -= fade * Time.deltaTime;

		if (curCd > cooldown)
		{
			this.renderer.enabled = false;
			this.enabled = false;
		}
	}
}