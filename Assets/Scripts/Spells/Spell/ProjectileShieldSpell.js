﻿#pragma strict
#pragma implicit
#pragma downcast

private var curCd : float;
private var cooldown : float = 20;
private var fadeSpd : float;
private var targetLayer : int;
private var owner : NetworkViewID;
private var radius : float = 3.5;

function Awake ()
{
	fadeSpd = 1 / 1;
	ShutDown();
}

@RPC
function SetOwner (player : NetworkViewID)
{
	owner = player;
	var aux = NetworkView.Find(player);
	if (aux.gameObject.layer == TagManager.tagName.Bro)
		targetLayer = TagManager.GetLayerMask([TagManager.tagName.HoeProjectiles]);
	else
		targetLayer = TagManager.GetLayerMask([TagManager.tagName.BroProjectiles]);

	transform.parent = aux.transform;
	transform.localPosition = Vector3.zero;
	transform.localRotation = Quaternion.identity;
	gameObject.layer = aux.gameObject.layer + 2;
	curCd = cooldown;
}

@RPC
function Set ()
{
	renderer.material.color.a = 0;
	renderer.enabled = true;
	enabled = true;
	curCd = 0;
}

function ShutDown ()
{
	renderer.enabled = false;
	enabled = false;
}

function Update ()
{
	renderer.material.color.a -= fadeSpd * Time.deltaTime;
	if (renderer.material.color.a < 0)
		renderer.material.color.a = 0;

	curCd += Time.deltaTime;
	if (curCd > cooldown && renderer.material.color.a == 0)
		ShutDown();

	if (Network.isServer && curCd < cooldown)
	{
		var colliders = Physics.OverlapSphere(transform.position, radius, targetLayer);
		for (var other : Collider in colliders)
		{
			var spell = other.gameObject.GetComponent(Spell);
			if (spell.dead)
				return;

			spell.dead = true;
			networkView.RPC("Counter", RPCMode.AllBuffered, other.transform.position, other.gameObject.networkView.viewID);
			other.gameObject.networkView.RPC("Death", RPCMode.AllBuffered);
		}
	}
}

@RPC
function Counter (hit : Vector3, netID : NetworkViewID)
{
	renderer.material.color.a = 1;
	if (networkView.isMine)
	{
		var obj = NetworkView.Find(netID).gameObject;
		var spell = obj.GetComponent(ProjectileSpell);
		var direction : Vector3;
		var player = PlayerManager.instance;
		if (spell.sendDirection)
		{
	 		direction = hit - player.transform.position;
	 		direction.y = 0;
	 		direction.Normalize();
		}
		else
			direction = hit;
		
		var clone : GameObject = SpellPool.instance.GetObj (spell.SpellName);
		clone.transform.position = hit;
		clone.networkView.RPC(	"SetProjectile",
								RPCMode.AllBuffered,
								gameObject.layer - 1,
								direction,
								player.GetStats (), 
								player.GetAtkModifiers(),
								player.dmg,
								owner,
								spell.lvl);
	 	spell = clone.GetComponent(Spell);
	 	SpellsManager.instance.KeepTrack (spell);
	}
}