#pragma strict
#pragma implicit
#pragma downcast

public class Spell extends UnitManager
{
	public static final var effectCount : int = 7;
	
	protected var spellName : String;
	var lvl : int;
	//var hp : float;
	
	var pushVelocity : float;
	var pushDeacceleration : float;
	var pushType : boolean; // magical(false), pure(true)
	var baseDeltaHealthMul : Vector3;
	var baseDeltaHealthCons : float;
	var deltaHealth : float;
	var deltaType : boolean; // magical(false), pure(true)
	var experienceGain : Vector3; // HIT - player, building, proj (gives the other player)
	
	// 0 - effIndice, 1 - duration, 2 - extra, 3 - mod, 4 - num
	// 5 - location, 6 - prefab, 7 - callback
	var effects : Array = new Array();
	var proc : int = -1;
	
	protected var lvlDependency : Array = new Array();
	protected var owner : NetworkViewID;
	var dead : boolean = false;
	
	function Reset ()
	{
		if (proc != -1)
		{
			effects.Remove(GetEffect(proc));
			proc = -1;
		}
		dead = false;
		if (networkView.isMine)
			enabled = true;
		if(rigidbody)
		{
			rigidbody.detectCollisions = true;
			collider.enabled = true;
		}
	}
	
	function GetEffect (num : int) : System.Object[]
	{
		for (var eff : System.Object[] in effects)
			if (eff[0] == num)
				return eff;
				
		return null;
	}
	
	@RPC
	function SetSpell (playerStats : Vector3, atkModifiers : Vector3, dmg : float,  player : NetworkViewID, lvlT : int)
	{
		Reset ();
		
		lvl = lvlT;
		for (var aux : Array in lvlDependency)
		{
			var func : Function = aux[0];
			var const : float = aux[1];
			var mult : float = aux[2];
			func(const + mult * lvl);
		}
		
		deltaHealth = baseDeltaHealthMul.x * playerStats.x  +
					  baseDeltaHealthMul.y * playerStats.y  +
					  baseDeltaHealthMul.z * playerStats.z  +
					  baseDeltaHealthCons - dmg;
		owner = player;
	}
	
	@RPC
	function AddProc (duration : float, extra : float, mod : float, num : int, location : String, prefab : String, effect : int)
	{
		effects.Add([effect, duration, extra, mod, num, location, prefab, function (x,y) {}]);
	}
	
	@RPC
	function AddCrit (mul : float, const : float)
	{
		deltaHealth *= mul;
		deltaHealth -= const;
	}
	
	@RPC
	function ApplySpell (spellID : NetworkViewID, normal : Vector3, owner_ : NetworkViewID)
	{
		if (networkView.isMine)
		{
			if (owner != NetworkViewID.unassigned)
				NetworkView.Find(owner).gameObject.GetComponent(PlayerManager).AddExp(normal.y);
			GotHit (2);
		}
	}
	
	function GotHit (type : int)
	{
		networkView.RPC("Death", RPCMode.All);
		enabled = false;
	}
	
	function DeathAnimation ()
	{
	}
	
	@RPC
	function Death ()
	{
		DeathAnimation();
		transform.position = Vector3 (-1000, -1000, -1000);
		rigidbody.detectCollisions = false;
		collider.enabled = false;
		if (networkView.isMine)
		{
			SpellsManager.instance.LoseTrack(this);
			Network.RemoveRPCs(networkView.viewID);
			SpellPool.instance.ReturnObj(spellName, gameObject);
		}
	}
	
	@RPC
	function Hited (layer : int, expGain : float)
	{
		if (networkView.isMine)
		{
			if (owner == NetworkViewID.unassigned)
			{
				GotHit(0);
				return;
			}
			
			var player = NetworkView.Find(owner).gameObject.GetComponent(PlayerManager);
			if (layer == 8 || layer == 12)
			{
				player.AddExp(experienceGain.x);
				GotHit (0);
			}
			else if (layer == 11 || layer == 15)
			{
				player.AddExp(experienceGain.y);
				GotHit (1);
			}
			else if (TagManager.CheckIfSpell(layer))
			{
				player.AddExp(expGain);
				GotHit (2);
			}
		}
	}
	
	function ValidateApplySpell (obj : GameObject) : float
	{
		if (Network.isServer)
		{
			if (dead)
				return;
			
			var expGain : float = 0;
			var layer = obj.layer;
			var isDead : boolean;
			if (TagManager.CheckIfPlayer(layer))
				isDead = obj.GetComponent(PlayerManager).dead;
			else if (TagManager.CheckIfBuilding(layer))
				isDead = obj.GetComponent(PlayerManager).dead; // Mudar para torre shit
			else if (TagManager.CheckIfSpell(layer))
			{
				var aux = obj.GetComponent(Spell);
				isDead = aux.dead;
				aux.dead = true;
				expGain = aux.experienceGain.z;
			}
			
			if (isDead)
				return -1;
			return expGain;
		}
		return -1;
	}

	function get SpellName ()
	{
		return spellName;
	}
}