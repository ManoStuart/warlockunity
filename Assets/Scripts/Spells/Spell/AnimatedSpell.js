﻿#pragma strict
#pragma implicit
#pragma downcast

public class AnimatedSpell extends Spell
{
	@RPC
	function SetOwner (player : NetworkViewID)
	{
		owner = player;
	}

	@RPC
	function CheckCast (hit : Vector3, netID : NetworkViewID)
	{
	}

	@RPC
	function SetSpellAnimation (pos : Vector3, atkModifiers : Vector3, enemyPos : Vector3)
	{
	}

	@RPC
	function SetAndHit (pos : Vector3, playerStats : Vector3, atkModifiers : Vector3, dmg : float,
			lvl : int, enemy : NetworkViewID, normal : Vector3, layer : int, expGain : float)
	{
		var enemyManager = NetworkView.Find(enemy).gameObject;
		SetSpellAnimation(pos, atkModifiers, enemyManager.transform.position);
		SetSpell(playerStats, atkModifiers, dmg, owner, lvl);
		if (enemy.isMine && enemy != NetworkViewID.unassigned)
		{
			normal *= -1;
			normal.y = experienceGain.z;
			enemyManager.GetComponent(UnitManager).ApplySpell(networkView.viewID, normal, owner);
		}
		if (networkView.isMine)
			Hited(layer, expGain);
	}
	
	function GotHit (type : int)
	{
	}
}