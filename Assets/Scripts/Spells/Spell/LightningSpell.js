﻿#pragma strict
#pragma implicit
#pragma downcast

public class LightningSpell extends AnimatedSpell
{
	private var baseRange : float = 10;
	private var baseRadius : float = 0.4;
	private var layerMask : int;
	private var curCd : float = 0;
	private var cooldown : float = 0.3;

	function Awake ()
	{
		baseDeltaHealthCons = -5;
		pushVelocity = 40;
		experienceGain = new Vector3 (10, 10, 10);
		enabled = false;
	}

	@RPC
	function SetOwner (player : NetworkViewID)
	{
		super.SetOwner(player);
		var array : TagManager.tagName[];
		if (NetworkView.Find(player).gameObject.layer == TagManager.tagName.Hoe)
			array = [TagManager.tagName.Bro, TagManager.tagName.BroProjectiles, TagManager.tagName.BroBuildings];
		else
			array = [TagManager.tagName.Hoe, TagManager.tagName.HoeProjectiles, TagManager.tagName.HoeBuildings];
		
		layerMask = TagManager.GetLayerMask(array);
	}


	@RPC
	function SetSpellAnimation (pos : Vector3, atkModifiers : Vector3, enemyPos : Vector3)
	{
		transform.position = pos;
		transform.rotation = Quaternion.LookRotation((enemyPos - pos));
		transform.BroadcastMessage("SetDis", Mathf.Ceil((enemyPos - pos).magnitude));
		curCd = 0;
		enabled = true;	
	}

	@RPC
	function CheckCast (target : Vector3, pos : Vector3, netID : NetworkViewID, lvl : int)
	{
		if (Network.isServer)
		{
			var player = NetworkView.Find(netID).gameObject.GetComponent(PlayerManager);
			var hit : RaycastHit;
			var atkModifiers = player.GetAtkModifiers();
			var dir = target - pos;
			dir.y = 0;
			var mag = dir.magnitude;
			dir = dir.normalized;
			while (true)
			{
				if (Physics.SphereCast(pos, baseRadius * atkModifiers.z, dir, hit,
						baseRange * atkModifiers.x - baseRadius * atkModifiers.z, layerMask))
				{
					var obj = hit.collider.gameObject;
					var expGain : float = ValidateApplySpell(obj);
					if (expGain == -1)
					{
						obj.rigidbody.detectCollisions = false;
						obj.collider.enabled = false;
						continue;
					}

					dead = true;
					networkView.RPC("SetAndHit", RPCMode.AllBuffered, pos, player.GetStats(), atkModifiers,
						player.dmg, lvl, obj.networkView.viewID, hit.normal, obj.layer, expGain);
					return;
				}
				else
					break;
			}
			if (mag > baseRange * atkModifiers.x)
			mag = baseRange * atkModifiers.x;
			networkView.RPC("SetSpellAnimation", RPCMode.All, pos, atkModifiers, pos + dir * mag); 
		}
	}

	function Update ()
	{
		curCd += Time.deltaTime;
		if (curCd > cooldown)
		{
			transform.BroadcastMessage("ShutDown");
			this.enabled = false;
		}
	}
}