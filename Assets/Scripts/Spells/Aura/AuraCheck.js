﻿#pragma strict
#pragma implicit
#pragma downcast

// Allies, Enemies
private var inside : Array[] = new Array[2];
private var manager : AuraManager;
var range : int;

function Awake ()
{
	inside[0] = new Array();
	inside[1] = new Array();
	
	enabled = false;
}

function SetLayer ()
{
	gameObject.layer = transform.parent.gameObject.layer;	
}

function GetArray (ally : int)
{
	return inside[ally];
}

function SetManager (man : AuraManager)
{
	manager = man;
}

function OnTriggerEnter (other : Collider)
{
	if (other.gameObject.name.Contains("Character"))
	{
		if (other.gameObject.layer != gameObject.layer)
			AddObj(other.gameObject, 1);
		else
			AddObj(other.gameObject, 0);
	}
}

function OnTriggerExit (other : Collider)
{
	if (other.gameObject.name.Contains("Character"))
	{
		if (other.gameObject.layer != gameObject.layer)
			RemoveObj(other.gameObject, 1);
		else
			RemoveObj(other.gameObject, 0);
	}
}

function AddObj (obj : GameObject, array : int)
{
	inside[array].Add(obj);
	manager.ApplyAura(obj, AuraCheck.GetRange(range, array));
}

function RemoveObj (obj : GameObject, array : int)
{
	inside[array].Remove(obj);
	manager.RemoveAura(obj, AuraCheck.GetRange(range, array));	
}

static function GetRange (range : int, ally : int)
{
	return range * 2 + ally;
}