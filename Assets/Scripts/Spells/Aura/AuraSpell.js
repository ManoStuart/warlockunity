﻿#pragma strict
#pragma implicit
#pragma downcast

public class AuraSpell extends Spell
{
	var viewID = NetworkViewID.unassigned;
	// var effects : Array = new Array();
	// 0 - effIndice, 1 - duration, 2 - extra, 3 - mod, 4 - num
	// 5 - location, 6 - prefab, 7 - callback
	//protected var lvlDependency : Array = new Array();

	function Awake ()
	{
		pushVelocity = 0;
		deltaHealth = 0.001;
		experienceGain = Vector3.zero;
	}
	
	@RPC
	function LvlUp (lvl : int) {
		for (var aux : Array in lvlDependency)
		{
			var func : Function = aux[0];
			var const : float = aux[1];
			var mult : float = aux[2];
			func(const + mult * lvl);
		}
	}

	@RPC
	function AddLvlDependency (ind : int, const : float, mul : float)
	{
		lvlDependency.Add([function(num : float) {GetEffect(ind)[2] = num;}, const, mul]);
	}

	@RPC
	function AddAuraEffect (ind : int, auraName : String, ext : float, mod : float, num : int, bool : boolean)
	{
		var eff = GetEffect(ind);
		var auraCode = AuraManager.GetAuraCode(auraName);
		if (eff)
		{
			if (eff[1] == auraCode)
			{
				eff[2] = ext;
				eff[3] = mod;
				eff[4] = num;
				return;
			}
		}
		owner = gameObject.networkView.viewID;
		effects.Add([ind,auraCode,ext,mod,num,"","",function (x,y){}]);

		if(bool)
		{
			if (ext < 0)
				deltaHealth = -0.001;
		}
	}
}