﻿#pragma strict
#pragma implicit
#pragma downcast

public class AuraCast extends SpellCast
{
	var auraSpells : AuraSpell[] = new AuraSpell[2];
	var ranges : int[] = new int[2];
	protected var auraManager : AuraManager;
	
	function Awake ()
	{
		auraManager = GetComponent(AuraManager);
		castAnimation = -1;
		currentCD = 0;
		isTargeted = false;
		lvl = 0;
		// Remember AuraSpell.GetAuraCode (str : String)
	}

	function Cast (hit : Vector3, player : PlayerManager, netID : NetworkViewID, spells : SpellsManager)
	{
		// Disable aura?
	}
	
	function LvlUp ()
	{
		var aux = new int[2];
		aux[0] = ranges[0];
		aux[1] = ranges[1];

		super.LvlUp();
		for (var i = 0; i < 2; i++)
			if (auraSpells[i])
			{
				auraSpells[i].networkView.RPC("LvlUp", RPCMode.AllBuffered, lvl);
				auraManager.UpdateAura (auraSpells[i], aux[i], ranges[i]);
			}
	}
}