﻿#pragma strict
#pragma implicit
#pragma downcast


private static var auraCodes = {"ItemHp" : -1, "ItemArmor" : -2, "ItemNegArmor" : -3, "ItemNegAtkSpd" : -4,
								"ItemMovSpd" : -5, "ItemHpRegem" : -6,  "ItemRing0" : -7, "ItemRing1" : -8, "ItemRing2" : -9,
								"StunEffect" : -10, "SilenceEffect" : -11, "PauseEffect" : -12, "InvisibleEffect" : -13};
private static var curAuraCode = -7;
public static var codeThreshold = -900;
private static var tempCodes = -1000;
// [temp, str]  -- temp code given to str;
private static var tempArray = new Array ();

var spells : Array[] = new Array[6]; // Allied / Enemy - from min range to max
var auraChecks : AuraCheck[] = new AuraCheck[3];

static var instance : AuraManager;

function Awake ()
{
	for (var i = 0; i < 6; i++)
		spells[i] = new Array();
	
	auraOn = true;
	if (networkView.isMine)
		AuraManager.instance = this;
}

function Start ()
{
	for (var i = 0; i < 3; i ++)
		auraChecks[i].SetManager (this);
	enabled = false;
}

// array -- %2 = (0)ally/(1)Enemy  /2 = range
function AddSpell (spell : AuraSpell, array : int)
{
	spells[array].Add(spell);
	for (var obj : GameObject in auraChecks[array/2].GetArray(array%2))
		obj.networkView.RPC("ApplySpell", RPCMode.All, spell.viewID, Vector3.zero, gameObject.networkView.viewID);
	
}

function RemoveSpell (spell : AuraSpell, array : int)
{
	spells[array].Remove(spell);
	for (var obj : GameObject in auraChecks[array/2].GetArray(array%2))
		for (var eff : System.Object[] in spell.effects)
		{
			var dur : float = eff[1];
			if (dur < 0)
				obj.networkView.RPC("PurgeAura", RPCMode.All, dur, eff[0]);
		}
}

function ApplyAura (obj : GameObject, array : int)
{
	for (var spell : AuraSpell in spells[array])
	{
		obj.networkView.RPC("ApplySpell", RPCMode.All, spell.viewID, Vector3.zero, gameObject.networkView.viewID);
	}
}

function RemoveAura (obj : GameObject, array : int)
{
	for (var spell : AuraSpell in spells[array])
		for (var eff : System.Object[] in spell.effects)
		{
			var dur : float = eff[1];
			if (dur < 0)
				obj.networkView.RPC("PurgeAura", RPCMode.All, dur, eff[0]);
		}
}

function UpdateAura (spell : Spell, before : int, after : int)
{
	RemoveSpell (spell, before);
	AddSpell (spell, after);
}

@RPC
function InstantiateAuraSpell (netId : NetworkViewID, ind : int, auraName : String, ext : float, num : int, array : int)
{
	var net = gameObject.AddComponent(NetworkView);
	var aura = gameObject.AddComponent(AuraSpell);
	net.observed = aura;
	net.stateSynchronization = NetworkStateSynchronization.Off;
	net.viewID = netId;
	aura.viewID = netId;
	aura.AddAuraEffect(ind, auraName, ext, 0, num, true);
	if (networkView.isMine)
	{
		yield;
		AddSpell(aura, array);
	}
}

@RPC
function InstantiateAuraSpell2 (netId : NetworkViewID, ind : int, auraName : String, ext : float, num : int, array : int,
								ext1 : float, num1 : int, ext2 : float, num2 : int)
{
	var net = gameObject.AddComponent(NetworkView);
	var aura = gameObject.AddComponent(AuraSpell);
	net.observed = aura;
	net.stateSynchronization = NetworkStateSynchronization.Off;
	net.viewID = netId;
	aura.viewID = netId;
	aura.AddAuraEffect(ind, auraName+0, ext, 0, num, true);
	aura.AddAuraEffect(ind, auraName+1, ext1, 0, num1, true);
	aura.AddAuraEffect(ind, auraName+2, ext2, 0, num2, true);
	if (networkView.isMine)
	{
		yield;
		AddSpell(aura, array);
	}
}

static function GetAuraCode (str : String) : float
{
	if (!AuraManager.auraCodes[str])
	{
		AuraManager.instance.networkView.RPC("SetServerAuraCode", RPCMode.Server, str);
		AuraManager.tempArray.Add([AuraManager.tempCodes, str]);
		AuraManager.tempCodes--;
		return AuraManager.tempCodes +1;
	}

	return AuraManager.auraCodes[str];
}

static function GetAuraCode (tempCode : float) : float
{
	var str : String = "";
	for (var aux : System.Object[] in AuraManager.tempArray)
		if (aux[0] == tempCode)
			str = aux[1];

	if (str == "")
		return;

	if (!AuraManager.auraCodes[str])
		return 1;
	else
		return AuraManager.auraCodes[str];
}

@RPC
function SetServerAuraCode (str : String)
{
	if (Network.isServer && !AuraManager.auraCodes[str])
	{
		Debug.Log("AuraCodeAdded");
		AuraManager.auraCodes[str] = AuraManager.curAuraCode;
		AuraManager.curAuraCode --;
		AuraManager.instance.networkView.RPC("SetAuraCode", RPCMode.OthersBuffered, str, AuraManager.curAuraCode +1);
	}
}

@RPC
function SetAuraCode (str : String, code : int)
{
	Debug.Log("AuraCodeAdded - Client");
	AuraManager.auraCodes[str] = code;
}