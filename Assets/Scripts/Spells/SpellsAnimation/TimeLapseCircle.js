﻿#pragma strict
#pragma implicit
#pragma downcast

private var maxHeight : float = 420;
private var speed : float = 8;

function Awake ()
{
	renderer.enabled = false;
	enabled = false;
}

function Reset ()
{
	transform.localPosition.y = maxHeight;
	speed = Random.Range(5, 11);
	renderer.enabled = true;
	enabled = true;
}

function ShutDown ()
{
	renderer.enabled = false;
	enabled = false;
}

function Update ()
{
	transform.position.y -= speed * Time.deltaTime;
	if (transform.localPosition.y < 0)
		ShutDown();
}