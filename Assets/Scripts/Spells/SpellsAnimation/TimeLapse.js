﻿#pragma strict
#pragma implicit
#pragma downcast

private var curCd : float;
private var cooldown : float = 1;

function Awake ()
{
	ShutDown();
}

@RPC
function SetOwner (player : NetworkViewID)
{
	var aux = NetworkView.Find(player);

	transform.parent = aux.transform;
	transform.localPosition = Vector3.zero;
	transform.localRotation = Quaternion.identity;
}

@RPC
function Set ()
{
	renderer.enabled = true;
	enabled = true;
	curCd = 0;
	transform.BroadcastMessage("Reset");
}

function ShutDown ()
{
	renderer.enabled = false;
	enabled = false;
}

function Update ()
{
	
	curCd += Time.deltaTime;
	if (curCd > cooldown)
		ShutDown();
}