﻿#pragma strict
#pragma implicit
#pragma downcast

public class LinkExtEffect extends ExtEffect
{
	private var line : LineRenderer;

	private var target : Transform;

	var pushForce : float = 1;
	var minRange : float = 10;
	var tickCD : float;
	var dmg : float;
	var dmgType : boolean;

	function Awake ()
	{
		line = (GetComponent(LineRenderer) as LineRenderer);
	}

	@RPC
	function SetOwner (player : NetworkViewID)
	{
		owner = player;
		var aux = NetworkView.Find(player);

		transform.parent = aux.transform.FindChild("Center");
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;
		line.SetPosition(0,Vector3.zero);
	}

	@RPC
	function Set (targ : NetworkViewID, bool : boolean)
	{
		hited = bool;
		renderer.enabled = true;
		enabled = true;
		target = NetworkView.Find(targ).transform;
		targetOwner = targ;
		
		if (Network.isServer)
		{
			if (hited)
				NetworkView.Find(targetOwner).RPC("AddExtEffect", RPCMode.All, networkView.viewID, true);
			else
				NetworkView.Find(owner).RPC("AddExtEffect", RPCMode.All, networkView.viewID, true);
		}
	}

	function Update ()
	{
		line.SetPosition(1, transform.InverseTransformPoint(target.position));
		
		if (hited)
		{
			if (Network.isServer)
			{
				if (CheckDist())
					networkView.RPC("ShutDown", RPCMode.All, 0);
			}
			
			if (targetOwner.isMine)
			{
				var dir = (transform.parent.position - target.position);
				dir.y = 0;
				dir.Normalize();
				
				PlayerManager.instance.Push(pushForce, 0, false, dir);
			}
		}
	}

	function CheckDist () : boolean
	{
		var dist = (transform.parent.position - target.position);
		dist.y = 0;
		if (dist.sqrMagnitude < minRange)
			return true;
		return false;
	}
}