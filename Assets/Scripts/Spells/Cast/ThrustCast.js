﻿#pragma strict
#pragma implicit
#pragma downcast

public class ThrustCast extends SpellCast
{
	private var spell : GameObject;
	private var baseSpeed : float = 50;
	private var motor : CharacterMotor;

	function Awake ()
	{
		if (networkView.isMine)
		{
			super.Awake();
			castAnimation = -1;
			cooldown = 0.5;
			lvl = 1;
			lvlUpCost = 5;
			// LVL Dependency
			texture = Resources.Load("textures/hud/spells/thrustTexture") as Texture;

			var prefab = Resources.Load("spellsPrefabs/Thrust") as GameObject;
			spell = Network.Instantiate(prefab, Vector3(-1000, -1000, -1000), Quaternion(-1000, -1000, -1000, -1000), 0);
			motor = GetComponent(CharacterMotor);
		}
		if (!networkView.isMine)
			enabled = false;
	}

	function Start ()
	{
		if (networkView.isMine)
			spell.networkView.RPC("SetOwner", RPCMode.AllBuffered, PlayerManager.instance.networkView.viewID);
	}

	function Cast (hit : Vector3, netID : NetworkViewID) : boolean
	{
		currentCD = cooldown;
		enabled = true;
		var player = PlayerManager.instance;
		var atkModifiers = player.GetAtkModifiers();
		spell.networkView.RPC("SetSpell", RPCMode.AllBuffered, player.GetStats(), atkModifiers, player.dmg, netID, lvl);
		var direction = (hit - transform.position);
		direction.y = 0;
		direction.Normalize();
		motor.SetVelocity(direction * baseSpeed * atkModifiers.y);
		return true;
	}
}