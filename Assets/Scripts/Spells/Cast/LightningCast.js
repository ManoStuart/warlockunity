﻿#pragma strict
#pragma implicit
#pragma downcast

public class LightningCast extends SpellCast
{
	private var spell : GameObject;
	private var spawnPoint : Transform;

	function Awake ()
	{
		if (networkView.isMine)
		{
			super.Awake();
			castAnimation = 2;
			castTime = 0.62;
			cooldown = 0.5;
			lvl = 1;
			lvlUpCost = 5;
			// LVL Dependency
			texture = Resources.Load("textures/hud/spells/lightningTexture") as Texture;

			spawnPoint = transform.Find("character/rig/root/hand_ik_L");
			var prefab = Resources.Load("spellsPrefabs/Lightning") as GameObject;
			spell = Network.Instantiate(prefab, Vector3(-1000, -1000, -1000), Quaternion(-1000, -1000, -1000, -1000), 0);
		}
		if (!networkView.isMine)
			enabled = false;
	}

	function Start ()
	{
		if (networkView.isMine)
			spell.networkView.RPC("SetOwner", RPCMode.AllBuffered, PlayerManager.instance.networkView.viewID);
	}

	function Cast (hit : Vector3, netID : NetworkViewID) : boolean
	{
		currentCD = cooldown;
		enabled = true;
		spell.networkView.RPC("CheckCast", RPCMode.AllBuffered, hit, spawnPoint.position, netID, lvl);
		return true;
	}
}