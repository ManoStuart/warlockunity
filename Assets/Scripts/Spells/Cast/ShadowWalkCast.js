﻿#pragma strict
#pragma implicit
#pragma downcast

public class ShadowWalkCast extends CritProc implements IOnCast
{
	private var fade : float;
	private var invDuration : float;
	private var casted : boolean;
	
	function Awake ()
	{
		fade = 1;
		invDuration = 15;

		seted = true;
		currentCD = 0;
		hasEffect = false;
		critConst = 30;
		critMul = 1;
		chance = 1;

		isTargeted = false;
		castAnimation = -1;
		castTime = 0.55;//.62
		cooldown = 20;
		lvl = 1;
		lvlUpCost = 5;
		// LVL Dependency
		texture = Resources.Load("textures/hud/spells/shadowWalkTexture") as Texture;

		if (!networkView.isMine)
			enabled = false;
		else
			enabled = true;
	}

	function Cast (hit : Vector3, netID : NetworkViewID) : boolean
	{
		PlayerManager.instance.ApplyEffect (PlayerManager.effect.invisibility, invDuration, fade, 0, 0, "", "",
			function (x,y)
			{
				TurnnOff();
			});
		SpellsManager.instance.AddProc(this);
		SpellsManager.instance.AddOnCast(this);

		casted = false;
		currentCD = cooldown;
		enabled = true;
		return false;
	}
	
	function OnCast(spellCast : SpellCast)
	{
		TurnnOff();
		PlayerManager.instance.PurgeEffect(PlayerManager.effect.invisibility);
	}

	function CheckProc (spell : Spell)
	{
		if (spell.deltaHealth > -5)
			return;

		AddCrit(spell);
	}

	function TurnnOff ()
	{
		if (casted)
			return;

		casted = true;
		SpellsManager.instance.AddRemOnCast(this);
		SpellsManager.instance.RemoveProc(this);
	}
}