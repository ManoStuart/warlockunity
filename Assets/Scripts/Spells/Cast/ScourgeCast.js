﻿#pragma strict
#pragma implicit
#pragma downcast

public class ScourgeCast extends SpellCast
{
	private var spell : GameObject;

	function Awake ()
	{
		if (networkView.isMine)
		{
			super.Awake();
			castAnimation = 3;
			castTime = 0.65;
			cooldown = 0.5;
			lvl = 1;
			lvlUpCost = 5;
			isTargeted = false;
			// LVL Dependency
			texture = Resources.Load("textures/hud/spells/scourgeTexture") as Texture;
			procable = true;

			var prefab = Resources.Load("spellsPrefabs/Scourge") as GameObject;
			spell = Network.Instantiate(prefab, Vector3(-1000, -1000, -1000), Quaternion(-1000, -1000, -1000, -1000), 0);
		}
		if (!networkView.isMine)
			enabled = false;
	}

	function Start ()
	{
		if (networkView.isMine)
			spell.networkView.RPC("SetOwner", RPCMode.AllBuffered, PlayerManager.instance.networkView.viewID);
	}

	function Cast (hit : Vector3, netID : NetworkViewID) : boolean
	{
		currentCD = cooldown;
		enabled = true;
		spell.networkView.RPC("CheckCast", RPCMode.AllBuffered, hit, Vector3.zero, netID, lvl);
		return true;
	}
}