﻿#pragma strict
#pragma implicit
#pragma downcast

public class ProjectileShieldCast extends SpellCast
{
	private var spell : GameObject;

	function Awake ()
	{
		if (networkView.isMine)
		{
			super.Awake();
			isTargeted = false;
			castAnimation = -1;
			cooldown = 21;
			lvl = 1;
			lvlUpCost = 5;
			// LVL Dependency
			texture = Resources.Load("textures/hud/spells/projectileShieldTexture") as Texture;

			var prefab = Resources.Load("spellsPrefabs/ProjectileShield") as GameObject;
			spell = Network.Instantiate(prefab, Vector3(-1000, -1000, -1000), Quaternion(-1000, -1000, -1000, -1000), 0);
		}
		if (!networkView.isMine)
			enabled = false;
	}

	function Start ()
	{
		if (networkView.isMine)
			spell.networkView.RPC("SetOwner", RPCMode.AllBuffered, PlayerManager.instance.networkView.viewID);
	}

	function Cast (hit : Vector3, netID : NetworkViewID) : boolean
	{
		currentCD = cooldown;
		enabled = true;
		spell.networkView.RPC("Set", RPCMode.AllBuffered);
		return true;
	}
}