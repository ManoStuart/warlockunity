﻿#pragma strict
#pragma implicit
#pragma downcast

public class EtherealJauntCast extends SpellCast
{
	function Awake ()
	{
		super.Awake();
		lvl = 1;
		cooldown = 0.5;
		isTargeted = false;
		// lvl Dependency
		lvlUpCost = 5;
		texture = Resources.Load("textures/hud/spells/etherealJauntTexture") as Texture;
		if (!networkView.isMine)
			enabled = false;
	}
	
	function Cast (hit : Vector3, netID : NetworkViewID) : boolean
	{
		var array = SpellsManager.instance.LiveProjectiles;
		for (var i = array.length -1; i >= 0; i--)
		{
			var spell : Spell = array[i];
			if (spell.dead)
				continue;

			var y = PlayerManager.instance.transform.position.y;
			PlayerManager.instance.transform.position = spell.transform.position;
			PlayerManager.instance.transform.position.y = y;
			spell.networkView.RPC("Death", RPCMode.AllBuffered);
			currentCD = cooldown;
			enabled = true;
			return true;
			// Animation shit
		}

		return false;
	}
}