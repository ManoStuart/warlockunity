﻿#pragma strict
#pragma implicit
#pragma downcast

public class HomingCast extends ProjectileCast
{	
	function Awake ()
	{
		super.Awake();

		projName = "spellsPrefabs/Hoaming";
		sendDirection = false;
		castAnimation = 1;
		castTime = 0.55;
		cooldown = 0.5;
		lvl = 1;
		lvlUpCost = 5;
		// LVL Dependency
		spawnPoint = transform.Find("SpawnSpell");
		texture = Resources.Load("textures/hud/spells/homingTexture") as Texture;
		procable = true;
		if (!networkView.isMine)
			enabled = false;
	}
}