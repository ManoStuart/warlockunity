#pragma strict
#pragma implicit
#pragma downcast

public class SpellCast extends MonoBehaviour
{
	protected var castAnimation : int;
	protected var castTime : float;
	protected var cooldown : float;
	protected var currentCD : float;
	protected var isTargeted : boolean;
	protected var lvl : int;
	protected var lvlDependency : Array = new Array();
	protected var lvlUpCost : float;
	var pos : int;
	
	// OnGui shit
	protected var texture : Texture;
	
	function Awake ()
	{
		castAnimation = -1;
		currentCD = 0;
		isTargeted = true;
		lvl = 0;
	}
	
	function Cast (hit : Vector3, netID : NetworkViewID) : boolean
	{
		return false;
	}
	
	function Update ()
	{
		currentCD -= Time.deltaTime;
		
		if (currentCD <= 0)
		{
			this.currentCD = 0;
			enabled = false;
		}
	}
	
	function LvlUp ()
	{
		lvl++;
		for (var aux : Array in lvlDependency)
		{
			var func : Function = aux[0];
			var const : float = aux[1];
			var mult : float = aux[2];
			func(const + mult * lvl);
		}
	}
	
	function isAvailable ()
	{
		if (this.currentCD <= 0 && lvl > 0)
			return true;
		else
			return false;
	}
	
	function NormalizedCD ()
	{
		if (cooldown == 0)
			return 0;
		return currentCD / cooldown;
	}
	
	function get LvlUpCost ()
	{
		return lvlUpCost;
	}
	
	function get Texture ()
	{
		return texture;
	}

	function get CastAnimation ()
	{
		return castAnimation;
	}

	function get IsTargeted ()
	{
		return isTargeted;
	}

	function get CastTime ()
	{
		return castTime;
	}
}