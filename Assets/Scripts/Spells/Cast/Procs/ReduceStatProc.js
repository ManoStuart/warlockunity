﻿#pragma strict
#pragma implicit
#pragma downcast

public class ReduceStatProc extends ProcSpellHit
{
	function Awake ()
	{
		super.Awake();
		cooldown = 5;
		SetProc();
	}

	function Update ()
	{
		super.Update();
		if (currentCD == 0)
			ResetProc(null);
	}

	function CheckProc (spell : Spell) : boolean
	{
		if (spell.deltaHealth < -5)
		{
			if (currentCD == 0)
				AddProc(spell);
			currentCD = cooldown;
			enabled = true;
		}
		return false;
	}
	
	function AddProc (spell : Spell)
	{
		PlayerManager.instance.SetBaseVariable(PlayerManager.stat.movementSpeed, -3, true);
	}
	
	function ResetProc (spell : Spell)
	{
		PlayerManager.instance.SetBaseVariable(PlayerManager.stat.movementSpeed, 3, true);
	}
}