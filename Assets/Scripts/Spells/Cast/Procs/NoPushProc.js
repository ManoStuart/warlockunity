﻿#pragma strict
#pragma implicit
#pragma downcast

public class NoPushProc extends ProcSpellHit
{
	var chance : float;
	var push : float;
	var lastSpell : Spell;
	
	function Awake ()
	{
		super.Awake();
		cooldown = 20;
		chance = 0.2;
		SetProc();
	}
	
	function CheckProc (spell : Spell) : boolean
	{
		if (currentCD <= 0 && Random.value <= chance && spell.pushVelocity > 0)
		{
			currentCD = cooldown;
			enabled = true;
			AddProc(spell);
			return true;
		}
		
		return false;
	}
	
	function AddProc (spell : Spell)
	{
		push = spell.pushVelocity;
		spell.pushVelocity = 0;
		lastSpell = spell;
	}
	
	function ResetProc (spell : Spell)
	{
		if (spell != lastSpell)
			return;
		
		spell.pushVelocity = push;
		lastSpell = null;
	}
}