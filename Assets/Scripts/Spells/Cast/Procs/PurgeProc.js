﻿#pragma strict
#pragma implicit
#pragma downcast

public class PurgeProc extends ProcSpellHit
{
	var effects : Array;
	private var lastSpell : Spell;
	
	function Awake ()
	{
		super.Awake();
		cooldown = 40;
		SetProc();
	}
	
	function CheckProc (spell : Spell) : boolean
	{
		if (spell.effects.Count > 0)
			for (var eff : System.Object[] in spell.effects)
			{
				var aux : float = eff[0];
				if (aux < 0)
					return false;
			}
		
		if (currentCD <= 0 && spell.deltaHealth < 0)
		{
			currentCD = cooldown;
			enabled = true;
			AddProc(spell);
			return true;
		}
		return false;
	}
	
	function AddProc (spell : Spell)
	{
		effects = spell.effects;
		spell.effects = null;
		lastSpell = spell;
	}
	
	function ResetProc (spell : Spell)
	{
		if (spell != lastSpell)
			return;
		
		spell.effects = effects;
		lastSpell = null;
	}
}