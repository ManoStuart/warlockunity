﻿#pragma strict
#pragma implicit
#pragma downcast

public class CritProc extends ProcSpellCast
{
	protected var critMul : float;
	protected var critConst : float;
	protected var hasEffect : boolean;
	
	function Awake ()
	{
		super.Awake();
		hasEffect = false;
	}
	
	function SetVariables (chan : float, mul : float, const : float)
	{
		chance = chan;
		critMul = mul;
		critConst = const;
		SetProc();
	}
	
	function SetVariables (chan : float, dur : float, ext : float, mo : float, nu : int, loc : String, pref : String,  eff : int)
	{
		super.SetVariables(chan, dur, ext, mo, nu, loc, pref, eff);
		hasEffect = true;
	}
	
	function CheckProc (spell : Spell)
	{
		if (spell.deltaHealth > -5)
			return;

		Debug.Log("yeahhss");
		if (currentCD <= 0 && Random.value <= chance)
		{
			Debug.Log("yeahhsss444");
			currentCD = cooldown;
			enabled = true;
			AddCrit(spell);
			if (spell.effects.Count == 0 && hasEffect)
				AddProc(spell);
		}
	}
	
	function AddCrit (spell : Spell)
	{
		spell.networkView.RPC("AddCrit", RPCMode.AllBuffered, critMul, critConst);
	}
}