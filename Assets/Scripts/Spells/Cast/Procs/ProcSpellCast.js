#pragma strict
#pragma implicit
#pragma downcast

public class ProcSpellCast extends SpellCast
{
	protected var chance : float;
	protected var duration : float;
	protected var extra : float;
	protected var mod : float;
	protected var num : int;
	protected var location : String;
	protected var prefab : String;
	protected var effect : int;
	protected var seted : boolean;
	
	function Awake ()
	{
		cooldown = 0;
		currentCD = 0;
		enabled = false;
		seted = false;
	}
	
	function Cast (hit : Vector3, netID : NetworkViewID) : boolean
	{
		return false;
		// ONGUI SHIT
	}
	
	function LvlUp ()
	{
		SetProc();
		super.LvlUp();
	}
	
	function SetVariables (chan : float, dur : float, ext : float, eff : int)
	{
		SetVariables(chan, dur, ext, 0, 0, "", "", eff);
	}
	
	function SetVariables (chan : float, dur : float, ext : float, mo : float, nu : int, loc : String, pref : String,  eff : int)
	{
		chance = chan;
		duration = dur;
		extra = ext;
		mod = mo;
		num = nu;
		location = loc;
		prefab = pref;
		effect = eff;
		SetProc();
	}
	
	function CheckProc (spell : Spell)
	{
		if (spell.effects.Count > 0)
			return;

		if (currentCD <= 0 && Random.value <= chance)
		{
			currentCD = cooldown;
			enabled = true;
			AddProc(spell);
		}
	}
	
	function AddProc (spell : Spell)
	{
		spell.networkView.RPC("AddProc", RPCMode.AllBuffered, duration, extra, mod, num, location, prefab, effect);
	}
	
	function SetProc ()
	{
		if (seted)
			return;
			
		seted = true;
		SpellsManager.instance.AddProc(this);
	}
}