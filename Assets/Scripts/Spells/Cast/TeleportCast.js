﻿#pragma strict
#pragma implicit
#pragma downcast

public class TeleportCast extends SpellCast
{
	private var baseRange : float = 10;

	function Awake ()
	{
		super.Awake();
		lvl = 1;
		cooldown = 0.5;
		// lvl Dependency
		lvlUpCost = 5;
		texture = Resources.Load("textures/hud/spells/teleportTexture") as Texture;
		if (!networkView.isMine)
			enabled = false;
	}
	
	function Cast (hit : Vector3, netID : NetworkViewID) : boolean
	{
		hit.y = PlayerManager.instance.transform.position.y;
		var rangeMod = PlayerManager.instance.GetComponent(PlayerManager).GetAtkRange();
		var dir = hit - PlayerManager.instance.transform.position;
		if (dir.magnitude > baseRange * rangeMod)
			PlayerManager.instance.transform.position += dir.normalized * baseRange * rangeMod;
		else
			PlayerManager.instance.transform.position = hit;

		currentCD = cooldown;
		enabled = true;
		// Animation shit
		return true;
	}
}