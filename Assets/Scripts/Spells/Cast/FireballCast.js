#pragma strict
#pragma implicit
#pragma downcast

public class FireballCast extends ProjectileCast
{	
	function Awake ()
	{
		super.Awake();

		projName = "spellsPrefabs/Fireball";
		castAnimation = 1;
		castTime = 0.55;//.62
		cooldown = 0.5;
		lvl = 1;
		lvlUpCost = 5;
		// LVL Dependency
		spawnPoint = transform.Find("SpawnSpell");
		texture = Resources.Load("textures/hud/spells/fireballTexture") as Texture;
		procable = true;
		if (!networkView.isMine)
			enabled = false;
	}
}