﻿#pragma strict
#pragma implicit
#pragma downcast

public class TimeLapseCast extends SpellCast
{
	private var circles : GameObject;
	private var shadow : GameObject;
	private var shadowPos : Transform;
	private var retPos : Vector3;
	private var retRot : Quaternion;
	private var retVel : Vector3;
	private var retHp : float;
	private var lapseDuration : float = 3;
	private var curDuration : float;
	private var lapseOn : boolean;

	function Awake ()
	{
		if (networkView.isMine)
		{
			super.Awake();
			isTargeted = false;
			castAnimation = -1;
			cooldown = 5;
			lvl = 1;
			lvlUpCost = 5;
			// LVL Dependency
			texture = Resources.Load("textures/hud/spells/timeLapseTexture") as Texture;

			var prefab = Resources.Load("spellsPrefabs/TimeLapse") as GameObject;
			circles = Network.Instantiate(prefab, Vector3(-1000, -1000, -1000), Quaternion(-1000, -1000, -1000, -1000), 0);
			var prefab2 = Resources.Load("spellsPrefabs/Shadow") as GameObject;
			shadow = Instantiate(prefab2, Vector3(-1000, -1000, -1000), Quaternion(-1000, -1000, -1000, -1000));
			shadow.renderer.enabled = false;
			shadowPos = transform.Find("character");
			shadow.renderer.material.color = shadowPos.Find("character").renderer.material.color;
			shadow.renderer.material.color.a = 0.3;
		}
		if (!networkView.isMine)
			enabled = false;
	}

	function Start ()
	{
		if (networkView.isMine)
			circles.networkView.RPC("SetOwner", RPCMode.AllBuffered, PlayerManager.instance.networkView.viewID);
	}

	function Cast (hit : Vector3, netID : NetworkViewID) : boolean
	{
		shadow.transform.position = shadowPos.position;
		shadow.transform.rotation = shadowPos.rotation;
		shadow.transform.RotateAround (shadow.transform.position, Vector3.up, 90);
		shadow.renderer.enabled = true;

		retVel = rigidbody.velocity;
		retPos = transform.position;
		retRot = transform.rotation;
		retHp = PlayerManager.instance.Hp / PlayerManager.instance.MaxHp;

		curDuration = lapseDuration * PlayerManager.instance.GetAtkRange();
		if (curDuration > cooldown)
			cooldown = curDuration;
		lapseOn = true;
		currentCD = cooldown;
		enabled = true;
		return true;
	}

	function Update ()
	{
		super.Update();
		if (lapseOn)
		{
			curDuration -= Time.deltaTime;
			if (curDuration < 0)
			{
				shadow.renderer.enabled = false;
				lapseOn = false;

				if (PlayerManager.instance.GetEffectStatus(PlayerManager.effect.death))
					return;

				PlayerManager.instance.SetHealth(retHp, true);
				transform.position = retPos;
				transform.rotation = retRot;
				rigidbody.velocity = retVel;
				circles.networkView.RPC("Set", RPCMode.AllBuffered);
			}
		}
	}
}