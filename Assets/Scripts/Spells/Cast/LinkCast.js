﻿#pragma strict
#pragma implicit
#pragma downcast

public class LinkCast extends ProjectileCast
{
	private var link : GameObject;
	
	function Awake ()
	{
		super.Awake();

		projName = "spellsPrefabs/LinkProj";
		castAnimation = 1;
		castTime = 0.55;//.62
		cooldown = 2;
		lvl = 1;
		lvlUpCost = 5;
		// LVL Dependency
		spawnPoint = transform.Find("SpawnSpell");
		texture = Resources.Load("textures/hud/spells/fireballTexture") as Texture;
		procable = false;
		
		if (!networkView.isMine)
			enabled = false;
		else
		{
			var prefab = Resources.Load("spellsPrefabs/Link") as GameObject;
			link = Network.Instantiate(prefab, Vector3(-1000, -1000, -1000), Quaternion(-1000, -1000, -1000, -1000), 0);
		}
	}
	
	function Start ()
	{
		if (networkView.isMine)
			link.networkView.RPC("SetOwner", RPCMode.AllBuffered, PlayerManager.instance.networkView.viewID);
	}
}