﻿#pragma strict
#pragma implicit
#pragma downcast

public class ProjectileCast extends SpellCast
{
	protected var projectile : GameObject;
	protected var projName : String;
	protected var spawnPoint : Transform;
	protected var procable : boolean;
	protected var sendDirection : boolean;
	
	function Awake ()
	{
		super.Awake();
		sendDirection = true;
	}

	function Cast (hit : Vector3, netID : NetworkViewID) : boolean
	{
		var direction : Vector3;
		var player = PlayerManager.instance;
		if (sendDirection)
		{
	 		direction = hit - transform.position;
	 		direction.y = 0;
	 		direction.Normalize();
		}
		else
			direction = hit;
 		
 		var clone : GameObject = SpellPool.instance.GetObj (projName);
 		clone.transform.position = spawnPoint.position;
 		clone.transform.rotation = spawnPoint.rotation;
 		clone.networkView.RPC(	"SetProjectile",
	 							RPCMode.AllBuffered,
	 							gameObject.layer + 1,
	 							direction,
	 							player.GetStats (), 
	 							player.GetAtkModifiers(),
	 							player.dmg,
	 							netID,
	 							lvl);
	 	var spell = clone.GetComponent(Spell);
	 	SpellsManager.instance.KeepTrack (spell);
	 	if (procable)
	 		SpellsManager.instance.CheckProcs(spell);
		
		currentCD = cooldown;
		enabled = true;
		return true;
 	}
}