#pragma strict
#pragma implicit
#pragma downcast

private var safeGround : boolean;
private var lvlUp : boolean;

private var selected : SpellCast;
private var casting : SpellCast;
private var onHold : SpellCast;
private var hit : Vector3;
private var hitOnHold : Vector3;
private var aiming: boolean = false;
private var castTime : float;
private var castHold : float;
private var curTime : float;

private var animator : Animator;

private var disable : int; // 0 - no disable, 1 - May set onHold, 2 - FullDisable
private var effects : Array = new Array();

private var items : Item[] = new Item[5];
private var spells : SpellCast[] = new SpellCast[7];
private var liveProjectiles : Array = new Array();
private var procs : Array = new Array ();
private var onCast : Array = new Array ();
private var remOnCast : Array = new Array();

public static var instance : SpellsManager;

function Awake ()
{
	if (networkView.isMine)
	{
		SpellsManager.instance = this;
	}
}

function Start ()
{
	animator = GetComponentInChildren(Animator);
	spells[0] = GetComponent(FireballCast);
	spells[0].pos = 0;
	spells[1] = GetComponent(LightningCast);
	spells[1].pos = 1;
	spells[2] = GetComponent(LinkCast);
	spells[2].pos = 2;
	spells[3] = GetComponent(TeleportCast);
	spells[3].pos = 3;
	spells[4] = GetComponent(TimeLapseCast);
	spells[4].pos = 4;
	spells[5] = GetComponent(ShadowWalkCast);
	spells[5].pos = 5;
	spells[6] = GetComponent(ScourgeCast);
	spells[6].pos = 6;
	items[0] = GetComponent(Weapon);
	items[1] = GetComponent(Head);
	items[2] = GetComponent(Chest);
	items[3] = GetComponent(Accessory);
	items[4] = GetComponent(Feet);
	items[0].pos = 0;
	items[1].pos = 1;
	items[2].pos = 2;
	items[3].pos = 3;
	items[4].pos = 4;
	enabled = false;
}

function EnterSafeGround ()
{
	Deselect();
	safeGround = true;
	GUIManager.gui.SafeGround = true;
}

function LeaveSafeGround ()
{
	lvlUp = false;
	safeGround = false;
	GUIManager.gui.SafeGround = false;
	GUIManager.gui.Shopping = false;
}

function ToggleLvlUp ()
{
	if (!safeGround && !lvlUp)
	{
		// OnGUIShit
		return;
	}
	lvlUp = !lvlUp;
	GUIManager.gui.Shopping = lvlUp;
}

function Stop ()
{
	Deselect();
	casting = null;
	enabled = false;
}

function Cast (spell : SpellCast)
{
	if (enabled || disable == 1)
	{
		onHold = spell;
		return;
	}
	
	if (spell.CastAnimation == -1)
	{
		if (spell.Cast(hit, networkView.viewID))
			CheckOnCast(spell);
	}
	else
	{
		casting = spell;
		enabled = true;
		var str : String = "Base Layer.Cast " + spell.CastAnimation.ToString();
		StartCoroutine(AnimatorNetworkInterface.ToogleAnimator("SetInteger",
																spell.CastAnimation,
																"CastAnimation",
																str));
		castTime = -1;
		StartCoroutine(SetCasttime(str, spell.CastTime));
		curTime = 0;
	}
}

function SetCasttime (str : String, mul : float)
{
	var aux :  AnimatorStateInfo;
	do {
		aux = animator.GetCurrentAnimatorStateInfo(0);
		if (aux.IsName(str))
			break;
		aux = animator.GetNextAnimatorStateInfo(0);
		if (aux.IsName(str))
			break;
		yield;
	} while (true);
	
	castTime = aux.length;
	castHold = castTime * 0.85;
	castTime *= mul;
}

function Deselect ()
{
	GUIManager.gui.SelectSpell(-1);
	aiming = false;
	selected = null;
	onHold = null;
}

function GetCurItem (item : int) : System.Object[]
{
	return items[item].CurItem;
}

function PossibleItemTree (item : int, at : int) : Array[]
{
	return items[item].PossibleTree(at);
}

function UpgradeItem (item : int, upTo : int, gold : int) : boolean
{
	if (items[item].IsNextLvl(upTo))
		if (PlayerManager.instance.TryUseGold(gold))
		{
			items[item].SetLvl(upTo);
			return true;
		}
		
	return false;
}

function SelectSpell (x : int)
{
	if (safeGround && !lvlUp)
		return false;
	
	if (lvlUp)
	{
		if (PlayerManager.instance.TryUseExp(spells[x].LvlUpCost))
		{
			spells[x].LvlUp();
		}
		return true;
	}
	
	
	if (disable == 2)
	{
		//OnGuiShit
		return false;
	}
	
	var spell : SpellCast = spells[x];
	if (spell == null)
	{
		// OnGuiShit
		return false;
	}
	if (!spell.isAvailable ())
	{
		// OnGuiShit
		return false;
	}
	
	if (!spell.IsTargeted)
	{
		Cast(spell);
		return true;
	}
	
	GUIManager.gui.SelectSpell(x);
	
	selected = spell;
	aiming = true;
	return false;
}

function Aim (target : Vector3)
{
	if (!aiming || target == null)
		return false;
	
	// Cursor shit
	aiming = false;
	GUIManager.gui.SelectSpell(-1);
	
	if (enabled || disable == 1)
	{
		hitOnHold = target;
		Cast(selected);
		selected = null;
	}
	else
	{
		hit = target;
		Cast(selected);
		selected = null;
		GetComponent(SlowLookAt).SetTarget(hit);
	}
	
	return true;
}

function IsAiming ()
{
	return aiming;
}

function Update ()
{
	curTime += Time.deltaTime;
	if (castTime > 0)
	{
		if (casting != null && curTime > castTime)
		{
			if (casting.Cast(hit, networkView.viewID))
			{
				CheckOnCast(casting);
				casting = null;
			}
		}
		else if (curTime > castHold)
		{
			CastOnHold();
		}
	}
}

function CastOnHold ()
{
	enabled = false;
	if (onHold == null)
		return;
	
	hit = hitOnHold;
	Cast(onHold);
	if (onHold.IsTargeted)
		GetComponent(SlowLookAt).SetTarget(hitOnHold);
	onHold = null;
}

function SetDisable (dis : int, effect : Effect)
{
	var aux2 = dis;
	for (var temp : Effect in effects)
	{
		var aux = temp.OnDisableChange(dis, "SpellsManager");
		if (aux != -1)
		{
			dis = aux;
		}
	}
	
	if (disable != 0 && dis == 0)
	{
		disable = dis;
		CastOnHold();
	}
	else if (dis != 0 && dis > disable)
	{
		disable = dis;
		Stop();
	}
	
	if (aux2 != 0)
		effects.Add(effect);
	else
		effects.Remove(effect);
}

function AddProc (spellCast : ProcSpellCast)
{
	procs.Add(spellCast);
}

function CheckProcs (spell : Spell)
{
	for (var aux : ProcSpellCast in procs)
		aux.CheckProc(spell);
}

function RemoveProc (spellCast : ProcSpellCast)
{
	procs.Remove(spellCast);
}

function AddOnCast (spellCast : IOnCast)
{
	onCast.Add(spellCast);
}

function CheckOnCast (spellCast : SpellCast)
{
	var aux : IOnCast;

	for (aux in remOnCast)
		onCast.Remove(aux);
	remOnCast.Clear();

	for (aux in onCast)
		aux.OnCast(spellCast);
}

function AddRemOnCast (spellCast : IOnCast)
{
	remOnCast.Add(spellCast);
}

function RemoveOnCast (spellCast : IOnCast)
{
	onCast.Remove(spellCast);
}

function KeepTrack (clone : Spell)
{
	liveProjectiles.Add(clone);
}

function LoseTrack (clone : Spell)
{
	liveProjectiles.Remove(clone);
}

function get LiveProjectiles () : Array
{
	return liveProjectiles;
}

function GetNormalizedCDs ()
{
	var aux : float[] = new float[7];
	for (var i = 0; i < 7; i++)
	{
		if (spells[i])
			aux[i] = spells[i].NormalizedCD();
		else
			aux[i] = 0;
	}
	
	return aux;
}

function OnNetworkInstantiate (info : NetworkMessageInfo)
{
	if (!networkView.isMine)
		enabled = false;
}

function get LvlUp ()
{
	return lvlUp;
}

function GetSpellTexture (i : int)
{
	return spells[i].Texture;
}