﻿#pragma strict
function OnTriggerEnter (other : Collider)
{
	if (other.gameObject.layer == gameObject.layer && other.gameObject.name == "Character(Clone)")
		other.gameObject.GetComponent(SpellsManager).EnterSafeGround();
}

function OnTriggerExit (other : Collider)
{
	if (other.gameObject.layer == gameObject.layer && other.gameObject.name == "Character(Clone)")
		other.gameObject.GetComponent(SpellsManager).LeaveSafeGround();
}