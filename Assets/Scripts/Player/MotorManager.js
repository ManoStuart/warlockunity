#pragma strict
#pragma implicit
#pragma downcast

private var motor : CharacterMotor;
private var animator : Animator;
private var spells : SpellsManager;
private var lookAt : SlowLookAt;

private var destination : Vector3;
private var direction : Vector3;
private var target : int; // -1 = OnHold / 0 = idle / 1 = moving

private var disable : int; // 0 - no disable, 1 - May turn, 2 - FullDisable
private var effects : Array = new Array();

function Awake () {
	motor = GetComponent(CharacterMotor);
	animator = GetComponentInChildren(Animator);
	spells = GetComponent(SpellsManager);
	lookAt = GetComponent(SlowLookAt);
}

function Update ()
{	
	if (target == -1 && !spells.enabled)
			StartMoving();
 	
 	if (target == 1 && disable == 0)
 	{
 		if (motor.movement.velocity.magnitude * Time.fixedDeltaTime >= (destination - transform.position).magnitude)
 		{
 			transform.position = destination;
 			Stop();
			lookAt.enabled = false;
 		}
 		else
 		{
 			var temp = destination;
 			temp.y = transform.position.y;
 			direction = (temp - transform.position).normalized;
 		}
 	}
 	else if (target == 0 && disable == 0)
 	{
 		var animat = animator.GetCurrentAnimatorStateInfo(0);
 		if (animat.normalizedTime > 7 && animat.IsName("Base Layer.Idle") && !animator.IsInTransition(0))
 		{
 			var rand = 1; // Random and shit
 			StartCoroutine(AnimatorNetworkInterface.ToogleAnimator("SetInteger", 
																	rand,
																	"IdleAnimation",
																	"Base Layer.Idle " +rand.ToString()));
		}
 	} 	
 	
 	motor.inputMoveDirection = direction;
}

function SetDestination (dest : Vector3)
{
	if (dest == null)
		return;
	
	destination = dest;
	destination.y = transform.position.y; // Set this shit right
 	direction = Vector3.zero;
 	
	if (spells.enabled)
		target = -1;
	else if (disable == 0)
		StartMoving();
	else if (disable == 1)
		lookAt.SetTarget(destination);
}

function StartMoving ()
{
	if (destination.y == 1000)
		return;
	
	target = 1;
 	AnimatorNetworkInterface.SetAnimator ("SetBool" , true, "Moving");
	lookAt.SetTarget(destination);
}

function Stop ()
{
	direction = Vector3.zero;
	destination.y = 1000;
	target = 0;
 	AnimatorNetworkInterface.SetAnimator ("SetBool" , false, "Moving");
}

function SetDisable (dis : int, effect : Effect)
{
	var aux2 = dis;
	for (var temp : Effect in effects)
	{
		var aux = temp.OnDisableChange(dis, "MotorManager");
		if (aux != -1)
		{
			dis = aux;
		}
	}
	
	if (disable != 0 && dis == 0)
		StartMoving();
	else if (dis > disable)
		Stop();
	
	disable = dis;
	
	if (aux2 != 0)
		effects.Add(effect);
	else
		effects.Remove(effect);
}

function OnNetworkInstantiate (info : NetworkMessageInfo)
{
	if (!networkView.isMine)
		enabled = false;
}

function GetVelocity ()
{
	return motor.movement.velocity;
}

@script RequireComponent (CharacterMotor)