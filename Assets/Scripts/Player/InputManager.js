#pragma strict
#pragma implicit
#pragma downcast

private var motor : MotorManager;
private var spells : SpellsManager;
private var groundLayer : int;
private var playerLayer : int;

function Start () {
	motor = GetComponent(MotorManager);
	spells = GetComponent(SpellsManager);
	groundLayer = TagManager.GetLayerMask([TagManager.tagName.Ground]);
	playerLayer = TagManager.GetLayerMask([TagManager.tagName.Bro, TagManager.tagName.Hoe]);
}

function GetMouseHit (mask : LayerMask)
{
	var hit : RaycastHit;
	if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), hit, 1000, mask.value))
		return hit.point;
	else
		return null;
}

function GetSpellInput (i : int)
{
	if (spells.SelectSpell(i))
		motor.Stop();
}

function GetSpellInput ()
{
	for (var i = 0; i < 7; i++)
		if (Input.GetButtonDown("Spell"+ (i+1).ToString()))
		{
			if (spells.SelectSpell(i))
				motor.Stop();
			return true;
		}
		
	return false;
}

function LateUpdate ()
{
	var temp;
	if (Input.GetButtonDown("Stop"))
	{
		spells.Stop();
		motor.Stop();
		StartCoroutine(AnimatorNetworkInterface.ToogleAnimator("SetBool", 
																true,
																"Stop",
																"Base Layer.Idle"));
	}
	
	else if (GetSpellInput());
	
	else if (Input.GetButtonDown("LvlUp"))
		spells.ToggleLvlUp();
	
	else if (spells.LvlUp && Input.GetButtonDown("LvlUpStats"))
		GetComponent(PlayerManager).LvlUp();
	
	else if (GUIManager.gui.usableMouseInput)
	{
		if (Input.GetButtonDown ("Fire1"))
		{
			if (spells.IsAiming ())
			{
				temp = GetMouseHit(playerLayer);
				if (temp == null)
					temp = GetMouseHit(groundLayer);
				if (temp != null)
				{
					if (spells.Aim (temp))
						motor.Stop();
				}
			}
		}
		
		else if (Input.GetButtonDown ("Fire2"))
		{
			spells.Deselect();
			
			temp = GetMouseHit(groundLayer);
			if (temp != null)
				motor.SetDestination (temp);
		}
	}
}

function OnNetworkInstantiate (info : NetworkMessageInfo)
{
	if (!networkView.isMine)
		enabled = false;
}