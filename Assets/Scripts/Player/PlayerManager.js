#pragma strict
#pragma implicit
#pragma downcast

public class PlayerManager extends UnitManager
{
	private var animator : AnimatorNetworkInterface;
	private var input : InputManager;
	private var motor : CharacterMotor;
	private var controller : CharacterController;
	private var mesh : Renderer;
	
	private var effects : Effect[];
	private var ourExtEffects : Array = new Array();
	private var outExtEffects : Array = new Array();
	
	var dead : boolean;
	var Debugger : boolean;
	
	// Stats
	var heroName : String = "Bolar King";
	var heroNumber : int = -1;
	var teamNumber : int = 0;
	
	var baseMaxHp : Array = [100, 1, 1000000, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.maxHp, delta, 0);}];
	var maxHp : Array = [100, 0, 0, baseMaxHp, function (delta:float) {hp += delta; if (hp <= 0) hp = 0.1; maxH = maxHp[0];}];
	var hp : float = 100;
	var maxH : float;
	
	private var baseHpRegem : Array = [0.5, -1000, 1000, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.hpRegem, delta, 0);}];
	var hpRegem : Array = [0.5, 0, 0, baseHpRegem, function (delta:float) {}];
	
	private var hpPos : Transform;
	private var dmgTaken : float = 0;
	private var dmgInflicted : float = 0;
	
	private var expSum : float = 0;
	static var lvlUpExp : float = 100;
	private var exp : float = 0;
	private var lvl : int = 1;
	
	private var gold : float = 1000;
	private var unreliable : float = 0.2;
	private var goldGain : float = 1;
	private var unreliableGain : float = 0.01;
	
	//base - Base, Min, Max, percent, deltaReaction
	private var baseMovementSpeed : Array = [6.5, 2, 10, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.movementSpeed, delta, 0);}];
	
	//variable - current, extra, percent, base, deltaReaction
	private var movementSpeed : Array = [6.5, 0, 0, baseMovementSpeed, function (delta:float) {
			movSpd = movementSpeed[0];
			motor.movement.maxForwardSpeed = movSpd;
			AnimatorNetworkInterface.SetAnimator ("SetFloat" , movSpd, "MovSpd");}];
	var movSpd : float;
	
	private var groundAcceleration : float = 60;
	private var recoveryAcceleration : float = 0.8;
	
	private var baseAtkSpeed : Array = [1, 0.1, 5, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.atkSpeed, delta, 0);}];
	private var atkSpeed : Array = [1, 0, 0, baseAtkSpeed, function (delta:float) {
			atSp = atkSpeed[0];
			AnimatorNetworkInterface.SetAnimator ("SetFloat" , atSp, "AtkSpd");}]; 
	var atSp : float;
	
	
	private var baseAtkRange : Array = [1, 0.5, 2, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.atkRange, delta, 0);}];
	private var atkRange : Array = [1, 0, 0, baseAtkRange, function (delta:float) {atRan = atkRange[0];}];
	var atRan : float;
	
	private var baseProjectileSpeed : Array = [1, 0.1, 3, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.projectileSpeed, delta, 0);}];
	private var projectileSpeed : Array = [1, 0, 0, baseProjectileSpeed, function (delta:float) {projSpd = projectileSpeed[0];}];
	var projSpd : float;
	
	private var baseAreaEffect : Array = [1, 0.5, 3, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.areaEffect, delta, 0);}];
	private var areaEffect : Array = [1, 0, 0, baseAreaEffect, function (delta:float) {areaEff = areaEffect[0];}];
	var areaEff : float;
	
	private var baseArmor : Array = [1, -20, 1000, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.armor, delta, 0);}];
	private var armor : Array = [1, 0, 0, baseArmor, function (delta:float) {
			var aux : float; var aux2 : float; var arm : float = armor[0];
			aux = ArmorToDmgReduc(arm);
			aux2 = ArmorToDmgReduc(arm - delta);
			SetBaseVariable (PlayerManager.stat.dmgReduction, aux - aux2,	true);
			armo = armor[0];}]; // dmgReduction
	var armo : float;
	
	private var baseQuas : Array = [10, 1, 1000, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.quas, delta, 0);}];
	private var quas : Array = [10, 0, 0, baseQuas, function (delta:float) {
			SetBaseVariable (PlayerManager.stat.maxHp, 5 * delta, true);
			SetBaseVariable (PlayerManager.stat.hpRegem, 0.01 * delta, true);
			SetBaseVariable (PlayerManager.stat.movementSpeed, 0.1 * delta, true);
			qua = quas[0];}]; // HP , HPRegem , mov speed
	var quasGain : float = 1;
	var qua : float;
	
	
	private var baseWex : Array = [10, 1, 1000, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.wex, delta, 0);}];
	private var wex : Array = [10, 0, 0, baseWex, function (delta:float) {
			SetBaseVariable (PlayerManager.stat.armor, 0.14 * delta, true);
			SetBaseVariable (PlayerManager.stat.areaEffect, 0.05 * delta, true);
			SetBaseVariable (PlayerManager.stat.projectileSpeed, 0.05 * delta, true);
			we = wex[0];}]; // Armor , AreaEffect, proj spd
	var wexGain : float = 1;
	var we : float;
	
	private var baseExort : Array = [10, 1, 1000, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.exort, delta, 0);}];
	private var exort : Array = [10, 0, 0, baseExort, function (delta:float) {
			SetBaseVariable (PlayerManager.stat.atkRange, 0.05 * delta, true);
			SetBaseVariable (PlayerManager.stat.atkSpeed, 0.05 * delta, true);
			SetBaseVariable (PlayerManager.stat.knockbackReduction, 0.01 * delta, true);
			exo = exort[0];}]; // Range, AtkSpeed, knockreduction
	var exortGain : float = 1;
	var exo : float;
	
	private var baseDmgReduction : Array = [0.1, -100, 1, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.dmgReduction, delta, 0);}];
	private var dmgReduction : Array = [0.1, 0, 0, baseDmgReduction, function (delta:float) {dmgRed = dmgReduction[0];}];
	var dmgRed : float;
	
	private var baseKnockbackReduction : Array = [0, -100, 1, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.knockbackReduction, delta, 0);}];
	private var knockbackReduction : Array = [0, 0, 0, baseKnockbackReduction, function (delta:float) {knockRed = knockbackReduction[0];}];
	var knockRed : float;
	
	private var baseDamage : Array = [0, 0, 1000, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.damage, delta, 0);}];
	private var damage : Array = [0, 0, 0, baseDamage, function (delta:float) {dmg = damage[0];}];
	var dmg : float;
	
	private var baseLifeSteal : Array = [0, 0, 2, 0, function (delta:float) {ApplyDelta (PlayerManager.stat.lifeSteal, delta, 0);}];
	private var lifeSteal : Array = [0, 0, 0, baseLifeSteal, function (delta:float) {lifeSt = lifeSteal[0];}];
	var lifeSt : float;
	
	private var stats : Array[] = [maxHp, hpRegem, movementSpeed, atkSpeed, atkRange, projectileSpeed,
								 areaEffect, armor, quas, wex, exort, dmgReduction, knockbackReduction,
								 damage, lifeSteal];
	
	public enum stat {maxHp = 0, hpRegem = 1, movementSpeed = 2, atkSpeed = 3, atkRange = 4,
						projectileSpeed = 5, areaEffect = 6, armor = 7, quas = 8, wex = 9, exort = 10,
						dmgReduction = 11, knockbackReduction = 12, damage = 13, lifeSteal = 14};
	
	public enum effect {stun = 0, pause = 1, invisibility = 2, silence = 3, death = 4, stackable = 5, damageTime = 6};

	private var noDmg : boolean;
	private var noDmgEffects : Array = new Array();
	private var noPush : boolean;
	private var noPushEffects : Array = new Array();
	private var noEffects : boolean;
	private var noEffectsEffects : Array = new Array();
	private var noCollisionEffects : Array = new Array();
	
	private var lastHitter : NetworkViewID;
	private var procs : Array = new Array();
	private var resetProcs : Array = new Array();
	
	public static var instance : PlayerManager;
	
	function ArmorToDmgReduc (arm : float)
	{
		if (arm > 0) return (0.06 * arm) / (1 + 0.06 * arm);
		else return (Mathf.Pow(0.94, arm) - 1);
	}

	function Awake ()
	{
		if (networkView.isMine)
		{
			PlayerManager.instance = this;
		}
	
		heroNumber = -1;
		enabled = false;
		motor = GetComponent(CharacterMotor);
		input = GetComponent(InputManager);
		animator = GetComponentInChildren(AnimatorNetworkInterface);
		controller = GetComponent(CharacterController);
		hpPos = transform.Find("HealthHud").transform;
		mesh = transform.Find("character/character").renderer;
		// Effects shit
		effects = new Effect[Spell.effectCount];
		effects[0] = GetComponent(StunEffect);
		effects[1] = GetComponent(PauseEffect);
		effects[2] = GetComponent(InvisibleEffect);
		effects[3] = GetComponent(SilenceEffect);
		effects[4] = GetComponent(DeathEffect);
		effects[5] = GetComponent(StackableEffect);
		effects[6] = GetComponent(DamageTimeEffect);
				
		maxH = maxHp[0];
	}
	
	@RPC
	function ApplySpell (spellID : NetworkViewID, normal : Vector3, owner : NetworkViewID)
	{
		if (networkView.isMine)
		{
			var spell = NetworkView.Find(spellID).gameObject.GetComponent(Spell);
			CheckProcs(spell);
			
			if (spell.deltaHealth != 0)
				ChangeHealth (spell.deltaHealth, spell.deltaType, owner);
			
			if (spell.pushVelocity != 0 && !noPush)
				Push (spell.pushVelocity, spell.pushDeacceleration, spell.pushType, normal);
				
			if (!noEffects && spell.effects.Count > 0)
			{
				for (var eff : System.Object[] in spell.effects)
				{
					var array : int = eff[0];
					effects[array].AddEffect(eff[1], eff[2], eff[3],
											eff[4], eff[5], eff[6], eff[7]);
				}
			}
			
			ResetProcs(spell);
		}
	}
	
	function ApplyEffect (i : int, duration : float, extras : float, mod : float, num : int, path : String, prefab : String, callback : Function)
	{
		if (networkView.isMine)
		{
			if (noEffects)
				return;

			effects[i].AddEffect(duration, extras, mod, num, path, prefab, callback);
		}
	}
	
	@RPC
	function PurgeAura (auraCode : float, effect : int)
	{
		effects[effect].Purge(auraCode);
	}
	
	@RPC
	function AddExtEffect (viewID : NetworkViewID, our : boolean)
	{
		var ext = NetworkView.Find(viewID);
		if (our)
			ourExtEffects.Add(ext);
		else
			outExtEffects.Add(ext);
	}
	
	@RPC
	function PurgeExtEffect (viewID : NetworkViewID, our : boolean)
	{
		var array : Array = outExtEffects;
		var aux;
		if (our)
			array = ourExtEffects;
			
		for (var ext : NetworkView in ourExtEffects)
			if (ext.viewID == viewID)
				aux = ext;
				
		array.Remove(aux);
	}
	
	function MassPurge ()
	{
		for (var effect : Effect in effects)
			effect.Purge (0);
		
		for (var ext : NetworkView in ourExtEffects)
			ext.RPC("ShutDown", RPCMode.All, 1);
		for (var ext : NetworkView in outExtEffects)
			ext.RPC("ShutDown", RPCMode.All, 2);
			
		ourExtEffects.Clear();
		outExtEffects.Clear();
	}
	
	function PurgeEffect (effect : int)
	{
		effects[effect].Purge(0);
	}

	function SetHealth (num : float, percent : boolean)
	{
		if (percent)
			hp = maxH * num;
		else
		{
			hp = num;
			if (hp > maxH)
				hp = maxH;
			else if (hp <= 0)
			{
				hp = 0;
				Death();
			}
		}

	}

	function ChangeHealth (delta : float, type : boolean, owner : NetworkViewID)
	{
		if (delta < 0 && owner != NetworkViewID.unassigned)
			lastHitter = owner;

		if (noDmg)
			return;
	
		// GUI SHIT
		var mod : float = dmgReduction[0];
		mod = 1 - mod;
		if (type)
			mod = 1;
		
		var del = delta * mod;
		hp += del;
		if (delta < 0)
			dmgTaken += del;
		
		if (delta < 0 && owner != NetworkViewID.unassigned)
			NetworkView.Find(owner).networkView.RPC("DidDamage", RPCMode.AllBuffered, -1 * del, networkView.viewID);
		
		if (hp > maxH)
			hp = maxH;
		else if (hp <= 0)
		{
			hp = 0;
			Death();
		}
	}
	
	@RPC
	function DidDamage (num : float, netId : NetworkViewID)
	{
		if (networkView.isMine)
		{
			dmgInflicted += num;
			ChangeHealth(num * lifeSt, true, NetworkViewID.unassigned);
			// Save netID??!?
		}
	}
	
	function Push (velocity : float, deacceleration : float, type : boolean, direction : Vector3)
	{
		if (noPush)
			return;
		
		var mod : float= knockbackReduction[0];
		mod = 1 - mod;
		if (!type)
			mod = 1;
		
		direction.y = 0;
		direction.Normalize();
		var max : float = maxHp[0];
		motor.AddVelocity(direction * velocity *  (2 - Mathf.Pow( 0.99, (((max -hp)/ max)* 100) ) ) * mod );
		motor.movement.maxGroundAcceleration -= deacceleration;
		if (motor.movement.maxGroundAcceleration < 0)
			motor.movement.maxGroundAcceleration = 0;
		
	}
	
	function SetNoPush (dis : boolean, effect : Effect)
	{
		if (SetDisable(dis, effect, noPushEffects, "noPush"))
			noPush = dis;
	}
	
	function SetNoDmg (dis : boolean, effect : Effect)
	{
		if (SetDisable(dis, effect, noDmgEffects, "noDmg"))
			noDmg = dis;
	}
	
	function SetNoEffects (dis : boolean, effect : Effect)
	{
		if (SetDisable(dis, effect, noEffectsEffects, "noEffects"))
		{
			noEffects = dis;
			(effects[6] as DamageTimeEffect).noEffects = dis;
		}
	}
	
	function SetNoCollision (dis : boolean, effect : Effect)
	{
		if (SetDisable(dis, effect, noCollisionEffects, "noCollision"))
			networkView.RPC("SetDetectCollision", RPCMode.All, !dis);
	}
	
	@RPC
	function SetDetectCollision (bool : boolean)
	{
		controller.detectCollisions = bool;
	}
	
	function SetDisable (dis : boolean, effect : Effect, list : Array, str : String)
	{
		var aux : boolean = true;
		for (var temp : Effect in list)
		{
			if (temp.OnDisableChange(0, str) != -1)
			{
				aux = false;
				break;
			}
		}
		
		if (dis)
			list.Add(effect);
		else
			list.Remove(effect);
			
		return aux;
	}
	
	function Death ()
	{
		hp = maxHp[0];
		transform.position.y -= 100;
		MassPurge();
		
		ApplyEffect (PlayerManager.effect.death, 4, 0, 0, 0, "", "",
			function (x,y)
			{
				transform.position = GameObject.Find("spawnPoint").transform.position;
				enabled = true;
			});
		
		var lostGold = unreliable * gold;
		gold -= lostGold;
		unreliable = 0.2;
		lostGold += 30;
		if (lostGold < 100)
			lostGold = 100;
		
		var expGiven : float = 0.2 * expSum;
		if (expGiven < 100)
			expGiven = 100;
		
		if (lastHitter != NetworkViewID.unassigned)
			TeamsManager.teams.HeroKill(NetworkView.Find(lastHitter).GetComponent(PlayerManager).heroNumber, expGiven, lostGold);
		else
		{
			var team = -1;
			if (heroNumber <= 4)
				team = 10;
			TeamsManager.teams.HeroKill(team, expGiven, lostGold);
		}
		GUIManager.gui.SetHeroHp(Vector3(-1000, -1000, -1000), heroNumber, hp/maxH, mesh.enabled);
		enabled = false;
	}
	
	@RPC
	function HeroKill (exp : float, gold : float)
	{
		if (networkView.isMine)
		{
			AddExp(exp);
			AddGold(gold);
		}
	}
	
	function ChangeGoldGain (delta : float)
	{
		goldGain += delta;
	}
	
	function ChangeUnreliable (delta : float)
	{
		unreliable += delta;
	}

	function ChangeUnreliableGain (delta : float)
	{
		unreliableGain += delta;
	}
	
	function AddGold (num : float)
	{
		var aux = gold * unreliable;
		gold += num;
		aux += num;
		unreliable = aux / gold;
	}
	
	function TryUseGold (num : float) : boolean
	{
		if (gold < num)
			// GUIShit
			return false;
			
		gold -= num;
		return true;
	}
	
	function AddExp (num : float)
	{
		if (networkView.isMine)
		{
			exp += num;
			expSum += num;
		}
	}
	
	function TryUseExp (num : float) : boolean
	{
		if (exp < num)
			// GUIShit
			return false;

		exp -= num;
		return true;
	}
	
	function LvlUp () : boolean
	{
		if (TryUseExp(PlayerManager.lvlUpExp))
		{
			SetBaseVariable (PlayerManager.stat.quas, quasGain, true);
			SetBaseVariable (PlayerManager.stat.wex, wexGain, true);
			SetBaseVariable (PlayerManager.stat.exort, exortGain, true);
			lvl++;
			return true;
		}
		else
			return false;
	}
	
	function Update ()
	{
		if (networkView.isMine)
		{
			if (Debugger)
			{
				Debugger = false;
				AddExp(100);
				LvlUp();
				hp = maxH;
			}
			
			if (motor.movement.maxGroundAcceleration < groundAcceleration)
			{
				motor.movement.maxGroundAcceleration += recoveryAcceleration;
				if (motor.movement.maxGroundAcceleration > groundAcceleration)
					motor.movement.maxGroundAcceleration = groundAcceleration;
			}
			
			var regem : float = hpRegem[0];
			hp += regem * Time.deltaTime;
			if (hp > maxH)
				hp = maxH;
			if (hp < 0)
				Death ();
				
			gold += goldGain * Time.deltaTime;
			unreliable -= unreliableGain * Time.deltaTime;
			if (unreliable < 0.2)
				unreliable = 0.2;
			else if (unreliable > 0.6)
				unreliable = 0.6;
		}
		
		GUIManager.gui.SetHeroHp(hpPos.position, heroNumber, hp/maxH, mesh.enabled);
	}
	
	@RPC
	function InitializePlayer (heroNumberT : int, nameT : String )
	{
		if (heroNumberT > 4)
			gameObject.layer = 12;
		else
			gameObject.layer = 8;
			
		heroNumber = heroNumberT;
		heroName = nameT;
		TeamsManager.teams.players[heroNumber] = this;
		enabled = true;
		gameObject.BroadcastMessage("SetLayer", SendMessageOptions.DontRequireReceiver);
	}
	
	function OnNetworkInstantiate (info : NetworkMessageInfo) {
		//enabled = false;
	}
	
	function OnSerializeNetworkView(stream : BitStream, info : NetworkMessageInfo)
	{
		var health : float;
		var q : float;
		var e : float;
		var w : float;
		var arm : float;
		var gp : float;
		var maH : float;
		var ms : float;
		var asp : float;
		var ar : float;
		var ps : float;
		var ae : float;
		var dm : float;
		var ls : float;
		var dr : float;
		var kr : float;
		if (stream.isWriting)
		{
		    health = hp;
		    gp = gold;
		    q = qua;
		    e = exo;
		    w = we;
		    arm = armo;
		    maH = maxH;
		    ms = movSpd;
		    asp = atSp;
		    ar = atRan;
		    ps = projSpd;
		    ae = areaEff;
		    dm = dmg;
		    ls = lifeSt;
		    dr = dmgRed;
		    kr = knockRed;
		    stream.Serialize(health);
		    stream.Serialize(gp);
		    stream.Serialize(q);
		    stream.Serialize(e);
		    stream.Serialize(arm);
		    stream.Serialize(w);
		    stream.Serialize(maH);
		    stream.Serialize(ms);
		    stream.Serialize(asp);
		    stream.Serialize(ar);
		    stream.Serialize(ps);
		    stream.Serialize(ae);
		    stream.Serialize(dm);
		    stream.Serialize(ls);
		    stream.Serialize(dr);
		    stream.Serialize(kr);
		}
		else
		{
		    stream.Serialize(health);
		    stream.Serialize(gp);
		    stream.Serialize(q);
		    stream.Serialize(e);
		    stream.Serialize(arm);
		    stream.Serialize(w);
		    stream.Serialize(maH);
		    stream.Serialize(ms);
		    stream.Serialize(asp);
		    stream.Serialize(ar);
		    stream.Serialize(ps);
		    stream.Serialize(ae);
		    stream.Serialize(dm);
		    stream.Serialize(ls);
		    stream.Serialize(dr);
		    stream.Serialize(kr);
		    hp = health;
		    gold = gp;
		    qua = q;
		    exo = e;
		    we = w;
		    armo = arm;
		    maxH = maH;
		    movSpd = ms;
		    atSp = asp;
		    atRan = ar;
		    projSpd = ps;
		    areaEff = ae;
		    dmg = dm;
		    lifeSt = ls;
		    dmgRed = dr;
		    knockRed = kr;		    
		}    
	}
	
	function ResetVariable (i : int)
	{
		var variable : Array = stats[i];
	
		var base : Array = variable[3];
		var reaction : Function = variable[4];
		var num : float = variable[0];
		var baseNum : float = base[0];
		var aux = baseNum - num;
		variable[0] = baseNum;
		variable[1] = 0;
		variable[2] = 0;
		base[3] = 0;
		reaction(aux);
	}
	
	//variable - current, extra, percent, base, deltaReaction
	//base - Base, Min, Max, percent, deltaReaction
	function ApplyDelta (i : int, delta : float, percent : int) : float// percent - 0 = no, 1 = percent on current, 2 = percent on base
	{
		var variable : Array = stats[i];
	
		var current : float = variable[0];
		var initial : float = current;
		var extra : float = variable[1];
		var percentCurr : float = variable[2];
		var base : Array = variable[3];
		var reaction : Function = variable[4];
		var currBase : float = base[0];
		var min : float = base[1];
		var max : float = base[2];
		var percentBase : float = base[3];
		
		current += extra;
		current = current * 100 / ( 100 + percentCurr);
		current -= currBase * percentBase;	
		
		var aux : float = 0;
		switch (percent)
		{
			case 0:
				current += delta;
				break;
			case 1:
				percentCurr += delta;
				break;
			case 2:
				percentBase += delta;
				break;
		}
		
		current += currBase * percentBase / 100;
		current += current * percentCurr / 100;
		
		if (current < min)
		{
			extra = current - min;
			current = min;
		}
		else if (current > max)
		{
			extra = current - max;
			current = max;
		}
		else
			extra = 0;
		
		variable[0] = current;
		variable[1] = extra;
		variable[2] = percentCurr;
		base[3] = percentBase;
		
		reaction(current - initial);
		
		return aux;
	}
	
	function SetBaseVariable (i : int, num : float, delta : boolean)
	{
		var auxVar : Array = stats[i];
		var variable : Array = auxVar[3];
		
		var base : float = variable[0];
		var min : float = variable[1];
		var max : float = variable[2];
		var reaction : Function = variable[4];
		var aux : float = base;
		if (delta)
			base += num;
		else
			base = num;
			
		if (base > max)
			base = max;
		else if (base < min)
			base = min;
			
		variable[0] = base;
		reaction(base - aux);
	}
	
	function AddProc (spellCast : ProcSpellHit)
	{
		procs.Add(spellCast);
	}
	
	function CheckProcs (spell : Spell)
	{
		for (var aux : ProcSpellHit in procs)
			if (aux.CheckProc(spell))
				resetProcs.Add(aux);
	}
	
	function RemoveProc (spellCast : ProcSpellHit)
	{
		procs.Remove(spellCast);
	}
	
	function ResetProcs (spell : Spell)
	{
		for (var aux : ProcSpellHit in resetProcs)
			aux.ResetProc(spell);
	}
	
	function GetExort () : float
	{
		return exort[0];
	}
	
	function GetWex () : float
	{
		return wex[0];
	}
	
	function GetQuas () : float
	{
		return quas[0];
	}
	
	function GetStats () : Vector3
	{
		return Vector3(quas[0], wex[0], exort[0]);
	}
	
	function GetAtkRange () : float
	{
		return atkRange[0];
	}
	
	function GetProjectileSpeed () : float
	{
		return projectileSpeed[0];
	}
	
	function GetAreaEffect () : float
	{
		return areaEffect[0];
	}
	
	function GetAtkModifiers () : Vector3
	{
		return Vector3(atkRange[0], projectileSpeed[0], areaEffect[0]);
	}
	
	function GetAtkSpeed () : float
	{
		return atkSpeed[0];
	}
	
	function get Hp () : float
	{
		return hp;
	}
	
	function get MaxHp () : float
	{
		return maxHp[0];
	}
	
	function GetHpInfo () : Vector3
	{
		return Vector3 (hp, maxHp[0], hpRegem[0]);
	}
	
	function get LastHitter () : NetworkViewID
	{
		return lastHitter;
	}
	
	function get Exp ()
	{
		return exp;
	}
	
	function get Gold ()
	{
		return gold;
	}
	
	function get Lvl ()
	{
		return lvl;
	}
	
	function get DmgTaken ()
	{
		return dmgTaken;
	}

	function GetEffectStatus (num : int)
	{
		return effects[num].enabled;
	}
}