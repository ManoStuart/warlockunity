#pragma strict
private var animator : Animator;
private static var netAnimator : AnimatorNetworkInterface;

function Awake () {
	animator = GetComponent(Animator);
	if(networkView.isMine)
		netAnimator = this;
}

static function SwapController (path : String)
{
	AnimatorNetworkInterface.netAnimator.networkView.RPC("SwapController_", RPCMode.AllBuffered, path);
}

@RPC
function SwapController_ (path : String)
{
	var atkSpd = animator.GetFloat("AtkSpd");
	var movSpd = animator.GetFloat("MovSpd");
	animator.runtimeAnimatorController = RuntimeAnimatorController.Instantiate(Resources.Load(path)) as RuntimeAnimatorController;
	animator.SetFloat("AtkSpd",atkSpd);
	animator.SetFloat("MovSpd",movSpd);
}

@RPC
function SetBool (str : String, bool : boolean)
{
	animator.SetBool(str, bool);
}

@RPC
function SetFloat (str : String, aux : float)
{
	animator.SetFloat(str, aux);
}

@RPC
function SetInteger (str : String, aux : int)
{
	animator.SetInteger(str, aux);
}

static function ToogleAnimator (mode : String , i, variable : String, animation : String)
{
	netAnimator.networkView.RPC(mode, RPCMode.All, variable, i);
	
	while (!netAnimator.animator.GetNextAnimatorStateInfo(0).IsName(animation))
		yield;
	
	var aux;
	if (typeof(i) == int)
		aux = 0;
	else if (typeof(i) == boolean)
		aux = false;
	
	netAnimator.networkView.RPC(mode, RPCMode.All, variable, aux);
}

static function SetAnimator (mode : String , i, variable : String)
{
	netAnimator.networkView.RPC (mode, RPCMode.All, variable, i);
}