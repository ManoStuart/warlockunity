#pragma strict
public class StunEffect extends DisableEffect
{
	protected static var disableCode : float;

	private var spells : SpellsManager;
	private var motor : MotorManager;
	private var lookAt : SlowLookAt;
	private var currentAnimation : int = 0; // 0 - noAnimation, 1 - stun / 2 - push
	
	function Start ()
	{
		spells = GetComponent(SpellsManager);
		motor = GetComponent(MotorManager);
		lookAt = GetComponent(SlowLookAt);
		usualPath = "Effects";
		usualPrefab = "Stun";
		// hud texture

		if (networkView.isMine)
			StunEffect.disableCode = AuraManager.GetAuraCode("StunEffect");
		enabled = false;
	}

	function AddEffect (durationT : float, extra : float, mod : float, num : int, posPath : String, prefabT : String, callback : Function)
	{
		lookAt.enabled = false;
		currentAnimation = 0;
		SetAnimation();
		AnimatorNetworkInterface.ToogleAnimator("SetBool", true, "Toogle", "Base Layer.Effect " +currentAnimation.ToString());
		
		if (!enabled)
		{
			spells.SetDisable (1, this);
			motor.SetDisable (2, this);
			instantiatePrefab (posPath, prefabT);
		}
		
		if (durationT > 0)
		{
			if (durationT > duration)
				duration = durationT;
		}
		else if (durationT == StunEffect.disableCode)
			isAura ++;

		if (enabled && onPurgeCallback)
			onPurgeCallback (gameObject.GetComponent(PlayerManager), 4);

		enabled = true;
		onPurgeCallback = callback;
	}

	function Purge (friendly : int) // 0 - NormalDisable, 1 - friendly, 2 - Foe, 3 - FullDisable
	{
		super.Purge(friendly);
		
		if (friendly == StunEffect.disableCode)
		{
			isAura --;
			if (isAura <= 0)
			{
				isAura = 0;
				if (duration <= 0)
					Purge(0);
				
				return;
			}
		}

		if (friendly != 2 && isAura == 0)
		{
			duration = 0;
			enabled = false;
			spells.SetDisable (0, this);
			motor.SetDisable (0, this);
			AnimatorNetworkInterface.SetAnimator("SetInteger", 0, "EffectAnimation");
			DestroyPrefab ();
			if (onPurgeCallback)
				onPurgeCallback (gameObject.GetComponent(PlayerManager), friendly);
		}
	}
	
	function SetAnimation ()
	{
		if (motor.GetVelocity().magnitude == 0 && currentAnimation != 1)
		{
			AnimatorNetworkInterface.SetAnimator("SetInteger", 1, "EffectAnimation");
			currentAnimation = 1;
		}
		else if (motor.GetVelocity().magnitude != 0 && currentAnimation != 2)
		{
			currentAnimation = 2;
			AnimatorNetworkInterface.SetAnimator("SetInteger", 2, "EffectAnimation");
		}
	}
	
	function UpdateOverride ()
	{
		SetAnimation();
	}
	
	function OnDisableChange(newValue : int, component : String)
	{
		if (!enabled)
			return -1;
			
		if (component == "SpellsManager")
		{
			if (newValue > 0)
				return -1;
			
			return 1;
		}
		else if (component == "MotorManager")
		{
			if (newValue == 2)
				return -1;
			
			return 2;
		}
		
		return -1;
	}
}