#pragma strict
#pragma implicit
#pragma downcast

public class Effect extends MonoBehaviour
{
	function AddEffect (durationT : float, extra : float, mod : float, num : int, posPath : String, prefabT : String, callback : Function)
	{
	}

	function Purge (friendly : int) // 0 - FullDisable, 1 - friendly, 2 - Foe
	{
	}
	
	function OnDisableChange(newValue : int, component : String) : int
	{
	}
	
	function instantiatePrefab (posPath : String, prefabStr : String) : GameObject[]
	{
		if (prefabStr == "none" || prefabStr == "")
			return new GameObject[2];
	
		var prefab : GameObject = Resources.Load(prefabStr, GameObject);		
		
		var clone = Network.Instantiate(prefab, Vector3.zero, Quaternion.identity, 0);
		clone.networkView.RPC("SetParent",RPCMode.All, posPath, networkView.viewID);
		
		if (posPath.Contains("_L"))
		{
			var sideClone = Network.Instantiate(prefab, Vector3.zero, Quaternion.identity, 0);
			sideClone.networkView.RPC("SetParent",RPCMode.All, posPath.Replace("_L", "_R"), networkView.viewID);
		}
		
		return [clone, sideClone];
	}
	
	function DestroyPrefab (pref : GameObject)
	{
		if (pref == null)
			return;
		
		Network.RemoveRPCs(pref.networkView.viewID);
	    Network.Destroy(pref);
	}
}