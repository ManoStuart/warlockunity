﻿#pragma strict

public class DisableEffect extends Effect
{
	protected var duration : float;
	protected var isAura : int;
	protected var clone : GameObject;
	protected var sideClone : GameObject;
	protected var currentPrefab : String = "";
	protected var usualPath : String = "";
	protected var usualPrefab : String = "";
	protected var hudTexture : Texture;
	protected var onPurgeCallback : Function;

	function AddEffect (durationT : float, extra : float, mod : float, num : int, posPath : String, prefabT : String, callback : Function)
	{
	}

	function Purge (friendly : int) // 0 - FullDisable, 1 - friendly, 2 - Foe
	{		
		if (!enabled)
			return;
		
		if (friendly == 3)
		{
			isAura = 0;
			onPurgeCallback = null;
			Purge(0);
		}
	}
	
	function OnDisableChange(newValue : int, component : String) : int
	{
	}
	
	function instantiatePrefab (posPath : String, prefabT : String) : GameObject[]
	{
		var prefabStr : String;
		if (prefabT == "")
			prefabStr = usualPrefab;
		else
			prefabStr = prefabT; 
		
		if (prefabStr == "")
			return null;
		
		if (currentPrefab != "")
		{
			if (currentPrefab == prefabStr)
				return null;
			else
				DestroyPrefab();
		}
		currentPrefab = prefabStr;
		
		var aux : String;
		if (posPath == "")
			aux = usualPath;
		else
			aux = posPath;
		
		var ret : GameObject[] = super.instantiatePrefab(aux, prefabStr);
		clone = ret[0];
		sideClone = ret[1];
		return null;
	}
	
	function DestroyPrefab ()
	{
		super.DestroyPrefab(clone);
		clone = null;
		currentPrefab = "";
		
		super.DestroyPrefab(sideClone);
		sideClone = null;
	}
	
	function Update ()
	{
		duration -= Time.deltaTime;
		if (isAura != 0)
			return;

		if (duration <= 0)
		{
			duration = 0;
			Purge(0);
		}
	}
}