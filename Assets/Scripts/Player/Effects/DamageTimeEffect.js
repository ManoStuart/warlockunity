﻿#pragma strict
#pragma implicit
#pragma downcast

public class DamageTimeEffect extends Effect
{
	// Stores arrays - 0 - Duration or Aura Code, 1 - CurrentCD, 2 - CD for ticks, 
	// 3 - Damage, 4 - dmgType, 5 - Owner, 6 - isAura, 7 - callback function
	// 8 - prefab, 9 - sidePrefab
	// dmgType < 0 = pure // 1 - normal, 2 - percent on cur, 3 - percent on base
	var stacks : Array = new Array();
	var noEffects : boolean;
	protected var removeStacks : Array = new Array();
		
	// duration - Positivo = Normal, Negativo = Aura Code.
	// extra = Damage
	// mod = CD for ticks
	// dmgType < 0 = pure // 1 - normal, 2 - percent on cur, 3 - percent on base
	function AddEffect (durationT : float, extra : float, mod : float, num : int, posPath : String, prefabT : String, callback : Function)
	{
		var isAura : int = 0;
		if (durationT < 0)
		{
			var aux : int;
			var auraCode : float;
			for (var eff : System.Object[] in stacks)
			{
				aux = eff[6];
				auraCode = eff[0];
				if (auraCode == durationT && aux != 0)
				{
					eff[6] = aux +1;
					return;
				}
			}
			isAura = 1;
		}
		
		var pref : GameObject[] = instantiatePrefab(posPath, prefabT);
		stacks.Add(new Array(durationT, mod, mod, extra, num, PlayerManager.instance.LastHitter, isAura, callback, pref[0], pref[1]));
		enabled = true;
	}
	
	function Purge (friendly : int) // 0 - NormalDisable, 1 - friendly, 2 - Foe, 3 - FullDisable
	{
		if (!enabled)
			return;
		
		if (friendly == 3)
		{
			stacks.Clear();
			enabled = false;
			return;
		}

		var temp : Array;
		var aux : float;
		var isAura : int;
		if (friendly == 1)
		{
			for (temp in stacks)
			{
				aux = temp[3];
				if (aux < 0)
					removeStacks.Add(temp);
			}
		}
		else if (friendly == 2)
		{
			for (temp in stacks)
			{
				aux = temp[3];
				if (aux > 0)
					removeStacks.Add(temp);
			}
		}
		else if (friendly < 0)
		{
			for (temp in stacks)
			{
				aux = temp[0];
				isAura = temp[6];
				if (aux == friendly && isAura > 0)
				{
					isAura--;
					if (isAura == 0)
						removeStacks.Add(temp);
					else
						temp[6] = isAura;
					break;
				}
			}
		}	
		
		for (temp in removeStacks)
		{
			DestroyPrefab(temp[8] as GameObject);
			DestroyPrefab(temp[9] as GameObject);
			
			var callback : Function = temp[7];
			if (callback)
				callback(PlayerManager.instance, friendly);
			stacks.Remove(temp);
		}
				
		if (stacks.length == 0)
			enabled = false;
	}
	
	function Update ()
	{
		var durationT : float;
		var dmg : float;
		var currentCD : float;
		var isAura : int;
		var purge = false;
		var num : int;
		var dmgType : boolean = false;
		var owner : NetworkViewID;
		
		for (var temp : Array in stacks)
		{
			currentCD = temp[1];
			currentCD -= Time.deltaTime;
			temp[1] = currentCD;
			dmg = temp[3];
			num = temp[4];
			owner = temp[5];
			if (num < 0)
			{
				num *= -1;
				dmgType = true;
			}
			if (num == 2)
				dmg *= PlayerManager.instance.hp;
			else if (num == 3)
				dmg *= PlayerManager.instance.maxH;
			
			if (currentCD <= 0 && !noEffects)
			{
				PlayerManager.instance.ChangeHealth(dmg, dmgType, owner);
				temp[1] = temp[2];
			}
			
			isAura = temp[6];
			durationT = temp[0];
			if (isAura == 0)
			{
				durationT -= Time.deltaTime;
				temp[0] = durationT;
				if (durationT <= 0)
				{
					removeStacks.Add(temp);
					purge = true;
					continue;
				}
			}
			else if (durationT < AuraManager.codeThreshold)
			{
				var code = AuraManager.GetAuraCode(durationT);
				if (code != 1)
					temp[0] = code;
			}
		}
		
		if (purge)
			Purge (0);
	}
}