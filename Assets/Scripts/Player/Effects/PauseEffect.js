#pragma strict
public class PauseEffect extends DisableEffect
{
	protected static var disableCode : float;

	private var spells : SpellsManager;
	private var motor : MotorManager;
	private var lookAt : SlowLookAt;
	private var player : PlayerManager;
	private var mesh : MeshManager;
	private var isFriendly : boolean;
	
	function Start ()
	{
		spells = GetComponent(SpellsManager);
		motor = GetComponent(MotorManager);
		lookAt = GetComponent(SlowLookAt);
		player = GetComponent(PlayerManager);
		mesh = transform.Find("character/character").GetComponent(MeshManager);
		usualPath = "Center";
		usualPrefab = "Pause";
		// hud texture

		if (networkView.isMine)
			PauseEffect.disableCode = AuraManager.GetAuraCode("PauseEffect");
		enabled = false;
	}

	function AddEffect (durationT : float, extra : float, mod : float, num : int, posPath : String, prefabT : String, callback : Function)
	{
		if (!enabled)
		{
			spells.SetDisable (1, this);
			motor.SetDisable (2, this);
			instantiatePrefab (posPath, prefabT);
			player.SetNoDmg (true, this);
			player.SetNoPush (true, this);
			player.SetNoEffects (true, this);
			player.SetNoCollision(true, this);
			mesh.DisableMesh (true, this);
			lookAt.enabled = false;
			
			if (extra > 0)
				isFriendly = true;
			else
				isFriendly = false;
		}
		
		if (durationT > 0)
		{
			if (durationT > duration)
				duration = durationT;
		}
		else if (durationT == PauseEffect.disableCode)
			isAura ++;
		
		if (enabled && onPurgeCallback)
			onPurgeCallback (gameObject.GetComponent(PlayerManager), 4);

		enabled = true;
		onPurgeCallback = callback;
	}

	function Purge (friendly : int) // 0 - NormalDisable, 1 - friendly, 2 - Foe, 3 - FullDisable
	{
		super.Purge(friendly);
		
		if (friendly == PauseEffect.disableCode)
		{
			isAura --;
			if (isAura <= 0)
			{
				isAura = 0;
				if (duration <= 0)
					Purge(0);
				
				return;
			}
		}

		if (((isFriendly && friendly != 1) || (!isFriendly && friendly != 2)) && isAura == 0)
		{
			duration = 0;
			enabled = false;
			spells.SetDisable (0, this);
			motor.SetDisable (0, this);
			player.SetNoDmg (false, this);
			player.SetNoPush (false, this);
			player.SetNoEffects (false, this);
			player.SetNoCollision(false, this);
			mesh.DisableMesh (false, this);
			DestroyPrefab ();
			if (onPurgeCallback)
				onPurgeCallback (gameObject.GetComponent(PlayerManager), friendly);
		}
	}
	
	function UpdateOverride ()
	{
	}
	
	function OnDisableChange(newValue : int, component : String)
	{
		if (!enabled)
			return -1;
			
		if (component == "SpellsManager")
		{
			if (newValue > 0)
				return -1;
			
			return 1;
		}
		else if (component == "MotorManager")
		{
			if (newValue == 2)
				return -1;
			
			return 2;
		}
		
		return 0;
	}
}