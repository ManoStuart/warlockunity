#pragma strict
public class DeathEffect extends DisableEffect
{
	private var spells : SpellsManager;
	private var motor : MotorManager;
	private var lookAt : SlowLookAt;
	private var player : PlayerManager;
	private var mesh : MeshManager;
	
	function Start ()
	{
		spells = GetComponent(SpellsManager);
		motor = GetComponent(MotorManager);
		lookAt = GetComponent(SlowLookAt);
		player = GetComponent(PlayerManager);
		mesh = transform.Find("character/character").GetComponent(MeshManager);
		// hud texture
		enabled = false;
	}

	function AddEffect (durationT : float, extra : float, mod : float, num : int, posPath : String, prefabT : String, callback : Function)
	{
		if (!enabled)
		{
			spells.SetDisable (2, this);
			motor.SetDisable (2, this);
			player.SetNoDmg (true, this);
			player.SetNoPush (true, this);
			player.SetNoEffects (true, this);
			player.SetNoCollision(true, this);
			mesh.DisableMesh (true, this);
			lookAt.enabled = false;
		}
		duration = durationT;
		
		enabled = true;
		onPurgeCallback = callback;
	}

	function Purge (friendly : int) // 0 - NormalDisable, 1 - friendly, 2 - Foe, 3 - FullDisable
	{
		super.Purge(friendly);
		
		if ((friendly != 2))
		{
			duration = 0;
			enabled = false;
			spells.SetDisable (0, this);
			motor.SetDisable (0, this);
			player.SetNoDmg (false, this);
			player.SetNoPush (false, this);
			player.SetNoEffects (false, this);
			player.SetNoCollision(false, this);
			mesh.DisableMesh (false, this);
			DestroyPrefab ();
			if (onPurgeCallback)
				onPurgeCallback (gameObject.GetComponent(PlayerManager), friendly);
		}

		// Buyback?
	}
	
	function UpdateOverride ()
	{
	}
	
	function OnDisableChange(newValue : int, component : String)
	{
		if (!enabled)
			return -1;
			
		if (component == "SpellsManager")
		{
			return 2;
		}
		else if (component == "MotorManager")
		{
			return 2;
		}
		
		return 0;
	}
}