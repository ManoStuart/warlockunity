#pragma strict
public class InvisibleEffect extends DisableEffect
{
	protected static var disableCode : float;

	private var mesh : MeshManager;
	private var isFriendly : boolean;
	
	function Start ()
	{
		mesh = transform.Find("character/character").GetComponent(MeshManager);
		// hud texture

		if (networkView.isMine)
			InvisibleEffect.disableCode = AuraManager.GetAuraCode("InvisibleEffect");
		enabled = false;
	}

	function AddEffect (durationT : float, extra : float, mod : float, num : int, posPath : String, prefabT : String, callback : Function)
	{
		if (!enabled)
			mesh.SetInvisibility (true, this, 0.7/extra); // extra = fadeTime
		
		if (durationT > 0)
		{
			if (durationT > duration)
				duration = durationT;
		}
		else if (durationT == InvisibleEffect.disableCode)
			isAura ++;
		
		if (enabled && onPurgeCallback)
			onPurgeCallback (gameObject.GetComponent(PlayerManager), 4);

		enabled = true;
		onPurgeCallback = callback;
	}

	function Purge (friendly : int) // 0 - NormalDisable, 1 - friendly, 2 - Foe, 3 - FullDisable
	{
		super.Purge(friendly);
		
		if (friendly == InvisibleEffect.disableCode)
		{
			isAura --;
			if (isAura <= 0)
			{
				isAura = 0;
				if (duration <= 0)
					Purge(0);
				
				return;
			}
		}

		if (((isFriendly && friendly != 1) || (!isFriendly && friendly != 2)) && isAura == 0)
		{
			duration = 0;
			enabled = false;
			mesh.SetInvisibility (false, this, 0);
			if (onPurgeCallback)
				onPurgeCallback (gameObject.GetComponent(PlayerManager), friendly);
		}
	}
	
	function UpdateOverride ()
	{
	}
	
	function OnDisableChange(newValue : int, component : String)
	{
		if (!enabled)
			return -1;
			
		return 0;
	}
}