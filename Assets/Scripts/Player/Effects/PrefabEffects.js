#pragma strict
@RPC
function SetParent (path : String, player : NetworkViewID)
{
	if (path != "")
		transform.parent = NetworkView.Find(player).transform.Find(path);
	else
		transform.parent = NetworkView.Find(player).transform;
	
	transform.localPosition = Vector3.zero;
	transform.localRotation = Quaternion.identity;
}