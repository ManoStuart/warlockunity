﻿#pragma strict

public class SilenceEffect extends DisableEffect
{
	protected static var disableCode : float;

	private var spells : SpellsManager;
	
	function Start ()
	{
		spells = GetComponent(SpellsManager);
		usualPath = "Center";
		usualPrefab = "Silence";
		// hud texture

		if (networkView.isMine)
			SilenceEffect.disableCode = AuraManager.GetAuraCode("SilenceEffect");
		enabled = false;
	}

	function AddEffect (durationT : float, extra : float, mod : float, num : int, posPath : String, prefabT : String, callback : Function)
	{
		if (!enabled)
		{
			spells.SetDisable (2, this);
			instantiatePrefab (posPath, prefabT);
		}
		
		if (durationT > 0)
		{
			if (durationT > duration)
				duration = durationT;
		}
		else if (SilenceEffect.disableCode == durationT)
			isAura ++;

		if (enabled && onPurgeCallback)
			onPurgeCallback (gameObject.GetComponent(PlayerManager), 4);

		enabled = true;
		onPurgeCallback = callback;
	}

	function Purge (friendly : int) // 0 - NormalDisable, 1 - friendly, 2 - Foe, 3 - FullDisable
	{
		super.Purge(friendly);
		
		if (friendly == SilenceEffect.disableCode)
		{
			isAura --;
			if (isAura <= 0)
			{
				isAura = 0;
				if (duration <= 0)
					Purge(0);
				
				return;
			}
		}

		if (friendly != 2 && isAura == 0)
		{
			duration = 0;
			enabled = false;
			spells.SetDisable (0, this);
			DestroyPrefab ();
			if (onPurgeCallback)
				onPurgeCallback (gameObject.GetComponent(PlayerManager), friendly);
		}
	}
	
	function UpdateOverride ()
	{
	}
	
	function OnDisableChange(newValue : int, component : String)
	{
		if (!enabled)
			return -1;
			
		if (component == "SpellsManager")
			return 2;
		
		return 0;
	}
}