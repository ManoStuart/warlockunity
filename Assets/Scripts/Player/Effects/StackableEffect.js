#pragma strict
#pragma implicit
#pragma downcast

public class StackableEffect extends Effect
{
	// Stores arrays - 0 - Duration or Aura Code, 1 - delta to be done, 2 - PercentTipe
	// 3 - variableIndice, 4 - overtime Function, 5 - isAura, 6 - callback Function, 7 - Prefab, 8 - Side
	var stacks : Array = new Array();
	protected var removeStacks : Array = new Array();
	protected var stats : int[] = new int[15];
		
	// duration - Positivo = Normal, Negativo = Aura Code.
	// extra  = Delta
	// mod (X.Y)/ X - 0 = Delta normal, 1 = Percent normal, 2 - Delta perSeconds, 3 - percent perSeconds, 4 - Delta then reduce overtime, 5 - Percent then reduce overtime
	// 			/ Y - 0 = deltaFunction, 0.5 = deltaBaseFunciton
	// num = PlayerManager.stats -- indice
	function AddEffect (durationT : float, extra : float, mod : float, num : int, posPath : String, prefabT : String, callback : Function)
	{
		var percent = 0;
		if (Mathf.FloorToInt(mod)%2 != 0)
		{
			percent = 1;
			if (mod % 1 != 0)
				percent = 2;
		}
		
		var isAura = 0;
		if (durationT < 0)
		{
			var bool : int;
			var auraCode : float; 
			for (var temp : System.Object[] in stacks)
			{
				bool = temp[3];
				auraCode = temp[0];
				if (bool != 0 && auraCode == durationT)
				{
					temp[3] = bool + 1;
					return;
				}
			}
			isAura = 1;
		}
		
		PlayerManager.instance.ApplyDelta (num, extra, percent);
		
		var overtime : Function = null;
		if (mod >= 2)
		{
			if (mod >= 4)
				overtime = function () {PlayerManager.instance.ApplyDelta(num, -extra / durationT * Time.deltaTime, percent);
										return (-extra / durationT * Time.deltaTime);};
			else
				overtime = function () {PlayerManager.instance.ApplyDelta(num, extra*Time.deltaTime, percent);
										return (extra*Time.deltaTime);};
		}		
		
		stats[num] += 1;
		var pref : GameObject[] = instantiatePrefab(posPath, prefabT);
		stacks.Add(new Array(durationT, extra, percent, num, overtime, isAura, callback, pref[0], pref[1]));
		enabled = true;
	}
	
	function Purge (friendly : int)  // 0 - NormalDisable, 1 - friendly, 2 - Foe, 3 - FullDisable
	{
		if (!enabled)
			return;

		if (friendly == 3)
		{
			stacks.Clear();
			enabled = false;
			return;
		}
		

		var temp : Array;
		var aux : float;
		var isAura : int;
		if (friendly == 1)
		{
			for (temp in stacks)
			{
				aux = temp[1];
				if (aux < 0)
					removeStacks.Add(temp);
			}
		}
		else if (friendly == 2)
		{
			for (temp in stacks)
			{
				aux = temp[1];
				if (aux > 0)
					removeStacks.Add(temp);
			}
		}
		else if (friendly < 0)
		{
			for (temp in stacks)
			{
				aux = temp[0];
				isAura = temp[5];
				if (aux == friendly && isAura > 0)
				{
					isAura--;
					if (isAura == 0)
						removeStacks.Add(temp);
					else
						temp[6] = isAura;
					break;
				}
			}
		}	
		
		for (temp in removeStacks)
		{
			var num : int = temp[3];
			stats[num] -= 1;
			if (stats[num] == 0)
				PlayerManager.instance.ResetVariable(num);
				
			DestroyPrefab(temp[7] as GameObject);
			DestroyPrefab(temp[8] as GameObject);
			
			var callback : Function = temp[6];
			if (callback)
				callback(PlayerManager.instance, friendly);
			stacks.Remove(temp);
		}

		if (stacks.length == 0)
			enabled = false;
	}
	
	function Update ()
	{
		var durationT : float;
		var extra : float;
		var percent : float;
		var overtimeFunc : Function;
		var isAura : int;
		var purge = false;
		var num : int;
		
		for (var temp : Array in stacks)
		{
			isAura = temp[5];
			extra = temp[1];
			durationT = temp[0];
			
			if (isAura == 0)
			{
				durationT -= Time.deltaTime;
				temp[0] = durationT;
				if (durationT <= 0)
				{
					percent = temp[2];
					num = temp[3];
					PlayerManager.instance.ApplyDelta(num, -extra, percent);
					removeStacks.Add(temp);
					purge = true;
					continue;
				}
			}
			else if (durationT < AuraManager.codeThreshold)
			{
				var code = AuraManager.GetAuraCode(durationT);
				if (code != 1)
					temp[0] = code;
			}

			if (temp[4] instanceof Function)
			{
				overtimeFunc = temp[4];
				var aux : float = overtimeFunc();
				extra += aux;
				temp[1] = extra;
			}
		}
		
		if (purge)
			Purge (0);
	}
}