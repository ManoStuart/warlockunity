﻿#pragma strict
#pragma implicit
#pragma downcast

public class ExtEffect extends MonoBehaviour
{
	protected var hited : boolean;
	protected var targetOwner : NetworkViewID;
	protected var owner : NetworkViewID;

	@RPC
	function SetOwner (player : NetworkViewID)
	{}

	@RPC
	function Set (targ : NetworkViewID, bool : boolean)
	{}

	@RPC
	function ShutDown (source : int) // 0 = Natural, 1 = owner, 2 = target
	{
		if (Network.isServer)
		{
			NetworkView.Find(owner).RPC("PurgeExtEffect", RPCMode.All, networkView.viewID, true);
			if (hited)
				NetworkView.Find(targetOwner).RPC("PurgeExtEffect", RPCMode.All, networkView.viewID, false);
		}
		
		renderer.enabled = false;
		enabled = false;
		targetOwner = NetworkViewID.unassigned;
	}
}