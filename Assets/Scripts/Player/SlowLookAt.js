#pragma strict
private var target : Vector3;
var deltaTurn : float = 0.2;

function SetTarget (aux : Vector3)
{
	if (aux == null)
		return;
	
	target = aux;
	enabled = true;
}

function Update ()
{
	var aux = target - transform.position;
	aux.y = transform.forward.y;
	var newRot = Vector3.RotateTowards(transform.forward, aux, deltaTurn, 0.0);
	transform.rotation = Quaternion.LookRotation(newRot);
}

function OnNetworkInstantiate (info : NetworkMessageInfo)
{
	if (!networkView.isMine)
		enabled = false;
}