#pragma strict
#pragma implicit
#pragma downcast

var items : SkinnedMeshRenderer[] = new SkinnedMeshRenderer[5]; // Weapon, Head, Chest, Accessory, Feet
var isDoubleSided : boolean[] = new boolean[5]; // Weapon, Head, Chest, Accessory, Feet
private var disableEffects : Array = new Array();
private var invisibilityEffects : Array = new Array();
private var normal : Shader;
private var transparent : Shader;
private var normalItems : Shader;
private var doubleItems : Shader;
private var team : boolean;
private var disable : boolean;
private var invisibility : boolean;
private var fade : float;
private var color : Color = Color.red;

public static var instance : MeshManager;

function Awake ()
{
	if (networkView.isMine)
		instance = this;
	
	normal = Shader.Find("Diffuse");
	transparent = Shader.Find("Transparent/Diffuse");
	normalItems = Shader.Find("Bumped Diffuse");
	doubleItems = Shader.Find("Bumped Diffuse Double Sided");
	disable = true;
}

function SetLayer ()
{
	gameObject.layer = transform.parent.parent.gameObject.layer;
	StartCoroutine(SetTeam());
}

function SetTeam ()
{
	team = true;
	if (!networkView.isMine)
	{
		var aux2 : int = -1;
		var aux : int = -1;
		var obj = transform.parent.parent.gameObject;
		var player : PlayerManager;
		
		do {
			player = obj.GetComponent(PlayerManager);
			yield;
		} while (!player);
		do {
			aux = player.heroNumber;
			yield;
		} while (aux == -1);
			yield;
		
		while (!GUIManager.gui)
			yield;
		do {
			aux2 = GUIManager.gui.mainPlayerTeam;
			yield;
		} while (aux2 == -1);
		
		if ((aux > 4 && aux2 <= 4) || ( aux <= 4 && aux2 > 4))
			team = false;
	}
}

function SetItemColor (materials : Material[])
{
	for (var mat : Material in materials)
		if (mat.name.Contains("Black"))
			mat.color = color;
}

@RPC
function AddMesh (mesh : String, pos : int, bool : boolean)
{
	var rend = transform.parent.FindChild(mesh).GetComponent(SkinnedMeshRenderer);
	rend.enabled = renderer.enabled;
	items[pos] = rend;
	isDoubleSided[pos] = bool;
	SetItemColor(rend.renderer.materials);
}

@RPC
function RemoveMesh (mesh : String, pos : int)
{
	var rem : SkinnedMeshRenderer;
	for (var item : SkinnedMeshRenderer in items)
		if (item.name == mesh)
		{
			rem = item;
			break;
		}
		
	items[pos].enabled = false;
	items[pos] = null;
}

function DisableMesh (dis : boolean, effect : Effect)
{
	if (SetDisable (dis, effect, disableEffects, "MeshManager"))
		disable = !dis;
		
	renderer.enabled = disable;
	for (var item : SkinnedMeshRenderer in items)
		if (item)
			item.renderer.enabled = disable;
}

function SetShader (materials : Material[], shader : Shader, alpha : float)
{
	for (var mat in materials)
	{
		mat.shader = shader;
		mat.color.a = alpha;
	}
}

function SetInvisibility (dis : boolean, effect : Effect, delta : float)
{
	if (SetDisable (dis, effect, invisibilityEffects, "Invisibility"))
	{
		invisibility = dis;
		fade = delta;
		if (!invisibility)
		{
			SetShader(renderer.materials, normal, 1);
			for (var i = 0; i < 5; i++)
			{
				if (items[i])
				{
					if (isDoubleSided[i])
						SetShader (items[i].renderer.materials, doubleItems, 1);
					else
						SetShader (items[i].renderer.materials, normalItems, 1);
				}
			}
		}
		else
		{
			SetShader(renderer.materials, transparent, renderer.material.color.a);
			for (var item : SkinnedMeshRenderer in items)
				if (item)
					SetShader(item.renderer.materials, transparent, renderer.material.color.a);
			enabled = true;
		}
	}
}

function SetDisable (dis : boolean, effect : Effect, list : Array, str : String)
{
	var aux : boolean = true;
	for (var temp : Effect in list)
	{
		if (temp.OnDisableChange(0, str) != -1)
		{
			aux = false;
			break;
		}
	}
	
	if (dis)
		list.Add(effect);
	else
		list.Remove(effect);
		
	return aux;
}

function SetAlpha (materials : Material[], num : float)
{
	for (var mat in materials)
		mat.color.a = num;
}

function ReduceAlpha (materials : Material[], red : float)
{
	for (var mat in materials)
		mat.color.a -= red;
}

function Update ()
{
	if (!invisibility)
	{
		enabled = false;
		return;
	}
	else if (renderer.material.color.a < 0.3)
	{
		SetAlpha (renderer.materials, 0.3);
		enabled = false;
		return;
	}

	var red = fade * Time.deltaTime;
	ReduceAlpha (renderer.materials, red);
	for (var item : SkinnedMeshRenderer in items)
		if (item)
			ReduceAlpha (item.renderer.materials, red);
}

function OnSerializeNetworkView(stream : BitStream, info : NetworkMessageInfo)
{
	var enab : boolean = true;
	var fad : float = 0;
	var invisib : boolean = false;
	if (stream.isWriting)
	{
		invisib = invisibility;
		fad = renderer.material.color.a;
		enab = disable;
		
	    stream.Serialize(enab);
	    stream.Serialize(fad);
	    stream.Serialize(invisib);
	}
	else
	{
	    stream.Serialize(enab);
	    stream.Serialize(fad);
	    stream.Serialize(invisib);
	    
	    if (!team && fad <= 0.3)
			enab = false;
		
	    renderer.enabled = enab;
		SetAlpha (renderer.materials, fad);
	    var shad : Shader;
	    var dbShad : Shader;
	    if (invisib)
	    {
	    	SetShader(renderer.materials, transparent, fad);
	    	shad = transparent;
	    	dbShad = transparent;
	    }
	    else
	    {
	    	SetShader(renderer.materials, normal, fad);
	    	shad = normalItems;
	    	dbShad = doubleItems;
	    }
	    
		for (var i = 0; i < 5; i ++)
		{
			if (items[i])
			{
				items[i].renderer.enabled = enab;
				if (isDoubleSided[i])
					SetShader (items[i].renderer.materials, dbShad, fad);
				else
					SetShader (items[i].renderer.materials, shad, fad);
			}
		}
	}    
}